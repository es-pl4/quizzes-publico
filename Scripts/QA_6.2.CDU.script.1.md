## TESTE DE QUALIDADE

**USER STORY/FEATURE:** 6-“Enquanto utilizador quero fazer login ou registar-me para aceder à Landing Page.“
“

**CASO DE USO:** 6.2 – “Registar”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de realizar o registo.

**TESTER:**

**Nº DE TESTE:**

**DATA:**


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| -> O utilizador preenche todos os campos da página registar **menos o nome** e clica no botão registar.| O sistema rejeita o registo ao detetar que os campos não estão todos preenchidos, mostrando a mensagem **"Preencha todos os campos!"**.|  |
| -> O utilizador preenche todos os campos da página registar **menos o email** e clica no botão registar.| O sistema rejeita o registo ao detetar que os campos não estão todos preenchidos, mostrando a mensagem **"Preencha todos os campos!"**.|  |
| -> O utilizador preenche todos os campos da página registar **menos o username** e clica no botão registar.| O sistema rejeita o registo ao detetar que os campos não estão todos preenchidos, mostrando a mensagem **"Preencha todos os campos!"**.|  |
| -> O utilizador preenche todos os campos da página registar **menos a password** e clica no botão registar.| O sistema rejeita o registo ao detetar que os campos não estão todos preenchidos, mostrando a mensagem **"Preencha todos os campos!"**.|  |
| -> O utilizador preenche todos os campos da página registar **menos a confirmação da password** e clica no botão registar.| O sistema rejeita o registo ao detetar que os campos não estão todos preenchidos, mostrando a mensagem **"Preencha todos os campos!"**.|  |
| -> O utilizador preenche todos os campos da página registar, utiliza um **email já registado na plataforma** e clica no botão registar.| O sistema rejeita o registo ao detetar que o **e-mail** está repetido e mostra a mensagem **"E-mail já registado!"**.|  |
| -> O utilizador preenche todos os campos da página registar, **não preenche corretamente o campo do email** e clica no botão registar.| O sistema rejeita o registo ao detetar que o **e-mail** está mal inserido e mostra a mensagem **"Inclua um "@" no endereço de email. Falta um "@" em "(caracteres inseridos no campo do email)"**.|  |
| -> O utilizador preenche todos os campos da página registar, utiliza um **username já registado na plataforma** e clica no botão registar.| O sistema rejeita o registo ao detetar que o **username** está repetido e mostra a mensagem **"E-mail já registado!"**.|  |
| -> O utilizador preenche todos os campos da página registar, e **insere passwords diferentes nos campos "password" e "confirmar password"** e clica no botão registar.| O sistema rejeita o registo ao detetar que as palavras-passes não coincidem e mostra a mensagem **"Passwords diferentes!"**.|  |
| -> O utilizador **não preenche todos os campos** da página registar e clica no botão registar.| O sistema rejeita o login ao detetar que os campos não estão todos preenchidos, mostrando a mensagem **"Preencha todos os campos!".**.|  |
| -> O utilizador **preenche todos os campos** da página registar e clica no botão registar.| O sistema apresenta a Página do Login e apresenta a mensagem **"Utilizador registado com sucesso"**.|  |
