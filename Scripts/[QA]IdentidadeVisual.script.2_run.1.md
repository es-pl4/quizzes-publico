## TESTE DE QUALIDADE
**IDENTIDADE VISUAL** - **MANUAL DE NORMAS:**
* 2.3. TIPOGRAFIA.

**ESPECIFICAÇÃO DO TESTE:**
* Verificação da aplicação correta da tipografia no website.

**TESTER:** xxxxxxxx
**Nº DE TESTE:** 1
**DATA:** xx-xx-xx

| AÇÃO | ESPERADO | RESUTADO |
|--|--|--|
| O **Tester** deve verificar se a tipografia esta devidamente aplicada no website segundo o manual de normas. | - **Texto** - “Poppins - Regular”; <br> - **Títulos das páginas** - “Poppins - Bold”; <br> - **Identificação do ID/TAG** - “Poppins - Semibold”; <br> - **Logotipo** - “Poppins - ExtraBold”.  | (aqui escreve-se OK ou NOK para dizer se aconteceu ou não) |
|Login Page|Tipografia|OK|
|Landing Page|Tipografia|OK|
|CriarQuiz|Tipografia|OK|
|ReverQuizList|Tipografia|OK|
|ReverQuiz|Tipografia|OK|
|CriarQuiz|Tipografia|OK|
|SelecionarTeste|Tipografia|OK|
|ResolverTeste|Tipografia|OK|
|VerSolucao|Tipografia|NOK (texto dentro de Chart)|
|CriarTeste|Tipografia|OK|
|--|--|
