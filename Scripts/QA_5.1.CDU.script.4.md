## TESTE DE QUALIDADE

 **USER STORY/FEATURE:** 5-“Enquanto utilizador autenticado quero uma landing page para aceder aos meus status e ações.“

**CASO DE USO:** 5.1 – “Ver Status”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de visualizar todos os dados relativos à percentagem que obteve em cada categoria).

**TESTER:**

**Nº DE TESTE:**

**DATA:**


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "Arquitetura e Design (A&D)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Arquitetura e Design (A&D)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "Risk management(RSK)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Risk management(RSK)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria " Testes e Qualidade de Produto (TST)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Testes e Qualidade de Produto (TST)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria " Verification and Validation (V&V)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria " Verification and Validation (V&V)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "Boas-práticas e Qualidade de Processos (PRC)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria " Boas-práticas e Qualidade de Processos (PRC)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "Configuration and Change Management (CCM)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Configuration and Change Management (CCM)" e o total de testes realizados na mesma.|  | 
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "Continuous practices (CI)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Continuous practices (CI)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "Deployment (DEP)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Deployment (DEP)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria " Gestão de projecto (PM)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Gestão de projecto (PM)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria " Implementação (IMP)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria "Implementação (IMP)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "  Peopleware (PPL)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria " Peopleware (PPL)" e o total de testes realizados na mesma.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado na categoria "Requisitos (REQ)" e o total de testes realizados na mesma**. |  O sistema apresenta os **dados relativos aos testes**: a que o utilizador foi aprovado  na categoria " Requisitos (REQ)" e o total de testes realizados na mesma.|  |
