## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 1. «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.1 – “Criar Teste” <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve testar se consegue proceder com a submissão do quiz, mesmo que esteja em falta campos de resposta ou a resposta correta por selecionar. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO (OK/NOK) |
|--|--|--|
| - O Tester clicou no botão "Criar Quiz" na página inicial. | - O sistema abriu a página de “Criar Quiz”.|  | |--|--|
|- O TESTER preencheu o campo de pergunta com "Não importa a falta de todas as respostas?" <br> - O Tester seleciona a sexta resposta como correta. <br> - O Tester seleciona a categoria "Gestão de Projeto". <br> O Tester clica no botão de "Submeter" para submeter o quiz.| - O sistema impede o quiz de ser submetido, alertando para preencher o primeiro campo de resposta e a justificação. |  |--|--|
| - O Tester preenche o primeiro campo de resposta com "Não". <br> - O Tester preenche o primeiro campo de justificação "A resposta 1 importa". <br> - O Tester clica no botão de "Submeter" para submeter o quiz. | - O sistema impede o quiz de ser submetido, alertando para preencher o segundo campo de resposta e a justificação. | |--|--|
| - O Tester preenche o segundo campo de resposta com "Não 2". <br> - O Tester preenche o segundo campo de justificação "A resposta 2 importa". <br> - O Tester preenche o terceiro campo de resposta com "Não 3". <br> - O Tester preenche o terceiro campo de justificação "A resposta 3 importa". <br> - O Tester clica no botão de "Submeter" para submeter o quiz. | - O sistema impede o quiz de ser submetido, alertando para preencher o quarto campo de resposta e a justificação. |  |--|--|
<br> - O Tester preenche o quarto campo de resposta com "Não 4". <br> - O Tester preenche o quarto campo de justificação "A resposta 4 é importante". <br> - O Tester preenche o quinto campo de resposta com "Não 5". <br> - O Tester preenche o quinto campo de justificação "A resposta 5 é importante". <br> - O Tester preenche o sexto campo de resposta com "Não 6". <br> - O Tester preenche o sexto campo de justificação "A resposta 6 é importante e correta". <br> O Tester clicou no botão de "Submeter" para submeter o quiz. | - O sistema procedeu com a aceitação do quiz. <br> - O sistema direcionou de novo para a página inicial. <br> - O sistema colocou o quiz na base de dados (onde consegue confirmar-se na lista da página inicial “Criação”)| |--|--|
