## TESTE DE QUALIDADE

 **USER STORY/FEATURE:** 5-“Enquanto utilizador autenticado quero uma landing page para aceder aos meus status e ações.“

**CASO DE USO:** 5.2 – “Ações”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de interagir (ou não em certas condições) com os botões.

**TESTER:**

**Nº DE TESTE:**

**DATA:**


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| O utilizador autenticado clica no botão **"Criar Quiz"**.| O utilizador autenticado é direcionado à **página de criação de quizzes**.|  |
| O utilizador autenticado clica no botão **"Rever Quiz"**.| O utilizador autenticado é direcionado à **página de revisão de quizzes**.|  |
| Caso o utilizador autenticado tenha criado no mínimo 1 quizz e revisto 3 passa a ter o **botão de resolver testes desbloqueado. Podendo assim clicar no mesmo**.| O utilizador autenticado é direcionado à **página de resolução de testes**.|  |
| Caso o utilizador autenticado não possua as qualificações referidas no ponto anterior é lhe apresentado um **botão não clicável**.| O utilizador autenticado **não consegue clicar no botão de resolver testes**.|  |
