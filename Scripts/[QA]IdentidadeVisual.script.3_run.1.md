## TESTE DE QUALIDADE
**IDENTIDADE VISUAL** - **MANUAL DE NORMAS:**
* 2.4.2. CORES - Website.

**ESPECIFICAÇÃO DO TESTE:**
* Verificação da aplicação correta das cores no website.

**TESTER:** xxxxxxxx
**Nº DE TESTE:** 1
**DATA:** xx-xx-xx

| AÇÃO | ESPERADO | RESUTADO |
|--|--|--|
| O **Tester** deve verificar se as cores estão devidamente aplicada no website segundo o manual de normas. | - **“HEX: #212121 - Cinza Escuro”** - todos os textos sobre fundo claro do website, como fundo da barra superior do site e como fundo da página de login e registo; <br> - **“HEX: #4CC86C - Verde”**, **“HEX: #F55D5D - Vermelho”** & **“HEX: #FFC83B - Amarelo”** - Landing Page para representar o estado aprovado/correto, reprovado/incorreto e por rever, respetivamente; <br> - **“HEX: #70AC5B - Verde Escuro”** & **“HEX: #AC5B5B - Vermelho Escuro”** - pagina de resultado do teste realizado para representar a aprovação ou reprovação de teste, respetivamente; <br> - **“HEX: #FFE27A - Gold”**, **“HEX: #A6A5A5 - Silver”** & **“HEX: #C99F81 - Bronze”** - representar o primeiro , segundo e terceiro lugar no Hall of Fame, respetivamente; <br> - **“HEX: #417FD8 - Azul”** - Usado na seleção da resposta correta no Criar Quizzes eResolver Testes. | (aqui escreve-se OK ou NOK para dizer se aconteceu ou não) |
|Login Page|Cores|OK|
|Landing Page|Cores|OK|
|CriarQuiz|Cores|OK|
|ReverQuizList|Cores|OK|
|ReverQuiz|Cores|OK|
|CriarQuiz|Cores|OK|
|SelecionarTeste|Cores|OK|
|ResolverTeste|Cores|OK|
|VerSolucao|Cores|OK|
|CriarTeste|Cores|OK|
|--|--|
