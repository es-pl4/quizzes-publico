## TESTE DE QUALIDADE
**USER STORY/FEATURE:** «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.3 – “Apagar Quiz” <br>
**ESPECIFICAÇÃO DO TESTE:** O TESTER deve confirmar se todos os campos são limpos ao clicar no botão “Limpar” na página de “Criar Teste” <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO (OK/NOK) |
|--|--|--|
| - O Tester clicou no botão "Criar Quiz" na página inicial. | - O sistema abriu a página de “Criar Quiz”.| | |--|--|
|- O Tester preenche o campo de pergunta com “O que é o Git?”. <br> - O tester preenche o primeiro campo de resposta com “Sistema de controle de versões distribuído” e a justificação com “Just1”. <br> - O tester preenche o segundo campo de resposta com ““Site de apostas online” e a justificação com “Just2”. <br> - O tester preenche o terceiro campo de resposta com “Sistema de inteligência artificial” e a justificação com “Just3”.<br> - O tester preenche o quarto campo de resposta com “Sistema de produção de criptomoeadas” e a justificação com “Just4”. <br> - O tester preenche o quinto campo de resposta com “Aplicação de bem-estar” e a justificação com “Just5”.<br> - O tester preenche o sexto campo de resposta com “Site de banco de dados” e a justificação com “Just6”.<br> - O Tester clica no botão "Limpar". | - O sistema "limpa" todos os campos de pergunta e resposta do quiz. | |--|--|
