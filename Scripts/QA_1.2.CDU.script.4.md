## TESTE DE QUALIDADE
**USER STORY/FEATURE:** «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.2 – “Editar Teste”  | Botão Cancelar <br>
**ESPECIFICAÇÃO DO TESTE:** O TESTER deve confirmar se, de facto, consegue, dentro da página de editar quiz, clicar no botão "cancelar" e retornar à landing page.  <br>
**TESTER:**  <br>
**Nº DE TESTE:**  <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO |
|--|--|--|
| - O Tester clica no botão “Reprovado” do quiz pretendido a editar na lista “Criação” na página principal.| - O sistema abriu a página de “Editar Quiz” com os campos já preenchidos do quiz pretendido a editar.|  | |--|--|
|- O Tester clicou no botão de "Cancelar" para voltar à landing page. | - O sistema redirecionou o utilizador de novo para a página inicial. |  |--|--|
