## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 4. «Enquanto utilizador autenticado, pretendo resolver as questões do quiz de forma a testar os meus conhecimentos.» <br>
**CASO DE USO:** 4.1 – “Resolver Teste” <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve testar se é possível resolver o quiz corretamente, selecionar as repostas e submeter as mesmas. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO |
|--|--|--|
| - O TESTER clicou no botão "Resolver Teste" na página inicial, sem cumprir os requisitos necessários para poder resolver um teste. <br> | - O sistema bloqueou o acesso à página de “Resolver Teste”.|  | |--|--|
| - O TESTER clicou no botão "Resolver Teste" na página inicial. <br> | - O sistema abriu a página de “Resolver Teste”.|  | |--|--|
|- O TESTER seleciona as respostas de cada quiz. <br> | - O sistema assume as respostas selecionadas, marcando-as, e fazendo com que seja desmarcada caso o utilizador selecione outra dentro do mesmo quiz. | |--|--|
|- O TESTER deixa uma ou mais respostas do quiz por responder. <br> | - O sistema assume as respostas não selecionadas, registando como não tendo sido respondidas. |  |--|--|
|- O TESTER termina o teste, clicando no botão "submeter". <br> | - O sistema assume as respostas selecionadas, reconhecendo-as como certas ou erradas, devolvendo esse resultado à base de dados.|  |--|--|
|- O TESTER termina o teste, clicando no botão "submeter". <br> | - O sistema redireciona o utilizador para a página da solução ao teste.|  |--|--|
|- O TESTER acede à landing page para verificar os resultados. <br> | - O sistema mostra o número de testes resolvidos, acrescentando o teste que o TESTER resolveu como certo ou errado. |  |--|--|
