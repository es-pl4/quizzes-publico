## TESTE DE QUALIDADE

**USER STORY / FEATURE:**
3.1 – Como utilizador autenticado quero ver a solução às respostas do quizz que realizei de forma a que forneça os conhecimentos pretendidos.

**CASO DE USO:** 3.1 - Visualizar se a resposta dada está correta e a respetiva justificação

**ESPECIFICAÇÃO DO TESTE:**
O tester deve ser capaz de visualizar todos os dados relativos ao teste que acabou de resolver (os quizzes pertencentes a esse teste; as perguntas, as respostas e sua justificação, para cada quizz) e os dados da sua pontuação final e estado de aprovado ou reprovado.

**TESTER:**<br>

**Nº TESTER:**<br>

**DATA:**<br>


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
|O utilizador autenticado tenta aceder à página **Ver Soluções sem ter resolvido um teste**.| O sistema **não mostra quaisquer resultados**, mesmo que o utilizador tenha feito testes anteriormente.||
|O utilizador autenticado carrega no botão **Resolver Testes**| O sistema deixa o utilizador **escolher o teste.**| |
|O utilizador autenticado **escolhe o teste Exame ES**.|O sistema mostra a página para **resolver o teste "Exame ES"**| |
|O utilizador autenticado **não responde** a nenhum dos quizzes. |– O sistema **reprova o teste, com uma pontuação final de 0 em 3,** na página de Ver Soluções. | |
|O utilizador autenticado **não responde a alguns dos quizzes e seleciona perguntas erradas noutros** (Exemplo: escolhe a terceira resposta nos quizzes 1,2 e não seleciona nada no terceiro quizz) | O sistema **reprova o teste, com uma pontuação final de 0 em 3**, na página de Ver Soluções.| |
|O utilizador autenticado **responde incorreto a todos os quizzes** (Exemplo: escolhe a terceira resposta em todos os quizzes) | O sistema **reprova o teste, com uma pontuação final de 0 em 3**, na página de Ver Soluções.| |
|O utilizador autenticado **erra 1 resposta, acerta 1 e não respondendo a um quizz** (Exemplo: escolhe a primeira resposta nos quizzes 1,2 e não seleciona nada no terceito quizz) | O sistema **reprova o teste, com uma pontuação final de 1 em 3**, na página de Ver Soluções.| |
|O utilizador autenticado **responde errado em duas perguntas e acerta 1** (Exemplo: escolhe a terceira resposta nos quizzes 1 e 2 e seleciona a primeira resposta no terceiro quizz) | O sistema **reprova o teste, com uma pontuação final de 1 em 3**, na página de Ver Soluções.| |
|O utilizador autenticado **acerta duas e não responde uma**(Exemplo: escolhe a primeira resposta nos quizzes 1 e 3 e não seleciona nenhuma resposta no terceiro quizz) | O sistema **aprova o teste, com uma pontuação final de 2 em 3**, na página de Ver Soluções. | |
|O utilizador autenticado **acerta duas e falha uma** (Exemplo: escolhe a primeira resposta em todos os quizzes do teste) | O sistema **aprova o teste, com uma pontuação final de 2 em 3**, na página de Ver Soluções.| |
|O utilizador autenticado **responde corretamente a todos os quizzes** (Exemplo: o utilizador escolhe a primeira resposta nos quizzes 1 e 3 e escolhe a segunda resposta no quizz 2) | O sistema **aprova o teste, com uma pontuação final de 3 em 3**, na página de Ver Soluções.| |
|O utilizador autenticado **clica em "SUBMETER"** o seu teste.| O sistema leva o utilizador para a **página Ver Soluções**.||
|O utilizador autenticado visualiza a secção do site correspondente à **visualização das soluções depois de ter resolvido um teste**. |	O utilizador vê, num **gráfico circular**, a sua pontuação, o número de perguntas certas no total das perguntas daquele teste e se está aprovado ou reprovado. |	|
|O utilizador autenticado carrega no botão **'PERFIL'**, para saltar a visualização de quais os quizzes corretamente respondidos. |	O sistema coloca o utilizador na **Landing Page**. |	|
|O utilizador autenticado pretende **ver com mais detalhe a sua performance no teste, dando scroll**. |	O sistema remove o gráfico circular e a informação geral do teste, passando a disponibilizar **a informação para cada quizz pela seguinte ordem: pergunta, respostas, justificação à resposta dada.** |	|
|O utilizador autenticado, ao dar scroll, pretende **ver as perguntas** que foram apresentadas no teste ao qual o mesmo respondeu. |	O sistema apresenta todas as **perguntas contidas nos quizzes apresentados** no teste resolvido. |	|
|O utilizador, ao dar scroll,  pretende **ver as possíveis respostas** que foram apresentadas no teste ao qual o mesmo respondeu. |	O sistema apresenta todas as **possíveis respostas contidas nos quizzes apresentados** no teste resolvido. |	|
|O utilizador, ao dar scroll,  pretende **ver as respostas certas dadas** pelo mesmo, no teste ao qual respondeu. |	O sistema apresenta a resposta certa dada pelo utilizador nos quizzes, através de uma caixa de **cor verde**. Todas as outras respostas contêm uma caixa de cor acinzentada. |	|
|O utilizador, ao dar scroll,  pretende **ver as respostas erradas dadas** pelo mesmo, no teste ao qual respondeu. |	O sistema apresenta a resposta errada dada pelo utilizador nos quizzes, através de uma caixa de **cor vermelha**. Todas as outras respostas contêm uma caixa de cor acinzentada. |	|
|O utilizador, ao dar scroll,  pretende **ver as respostas não respondidas** pelo mesmo, no teste ao qual respondeu. |	O sistema apresenta todas as respostas para aquele quizz numa **caixa acinzentada**, após demonstrar a informação da pergunta a elas relacionada. |	|
|O utilizador, ao dar scroll,  pretende a **justificação das respostas dadas** neste teste estarem certas ou incorretas. | 	O sistema, se existir uma resposta selecionada, **apresenta a justificação para aquela resposta escolhida**, após demonstrar a informação da pergunta e respostas a elas relacionada. |	|
|O utilizador autenticado ao acabar de ver as soluções do seu teste carrega no botão **‘PERFIL’**.|	O sistema redireciona o utilizador para a **Landing Page.** |	|
|O utilizador autenticado seleciona o botão **"Logout"**. |	O sistema permite ao utilizador **terminar sessão**.|	|

