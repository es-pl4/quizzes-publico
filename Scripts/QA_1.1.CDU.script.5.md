## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 1. «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.1 – “Criar Teste” | Botão Cancelar <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de, na página de criar quiz, clicar no botão "cancelar" e retornar à landing page. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO |
|--|--|--|
| - O TESTER clicou no botão "Criar Quiz" na página inicial. | - O sistema abriu a página de “Criar Quiz”.|  | |--|--|
| - O Tester clica no botão de "Cancelar" para voltar para a landing page. | - O sistema direcionou o utilizador para a página inicial |  |--|--|
