## TESTE DE QUALIDADE
**USER STORY/FEATURE:** «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.2 – “Editar Quiz” <br>
**ESPECIFICAÇÃO DO TESTE:** O TESTER deve confirmar se, de facto, consegue fazer alterações  à categoria e a resposta correta selecionadas num quiz já existente. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO (OK/NOK) |
|--|--|--|
| - O Tester clica no botão “Reprovado” do quiz pretendido a editar na lista “Criação” na página principal.| - O sistema abriu a página de “Editar Quiz” com os campos já preenchidos do quiz pretendido a editar.| | |--|--|
|- O Tester altera a categoria para "Peopleware". <br> - O Tester altera a resposta correta para a terceira resposta. <br> O Tester clicou no botão de "Submeter" para submeter o quiz. | - O sistema aceitou a submissão e direcionou de novo para a página principal. <br> - Na base de dados, a categoria e a resposta correta foram devidamente alteradas.| |--|--|
