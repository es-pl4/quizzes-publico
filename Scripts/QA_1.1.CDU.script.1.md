## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 1. «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.1 – “Criar Teste” <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de criar um quiz completo, preenchendo todos os campos de pergunta e resposta, a resposta correta e selecionar a categoria da pergunta, e deve conseguir submetê-lo com sucesso. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO |
|--|--|--|
| - O TESTER clicou no botão "Criar Quiz" na página inicial. | - O sistema abriu a página de “Criar Quiz”.|  | |--|--|
| - O TESTER preencheu o campo de pergunta com "Será que o teste 1 do caso 1_1 vai funcionar?" <br> - O Tester preenche o primeiro campo de resposta com "Sim". <br> - O Tester preenche o primeiro campo de justificação "Porque está correto". <br>  - O Tester preenche o segundo campo de resposta com "Talvez". <br> - O Tester preenche o segundo campo de justificação "Porque o código estará direito". <br>- O Tester preenche o terceiro campo de resposta com "Não". <br> - O Tester preenche o terceiro campo de justificação "Porque nunca corre bem". <br>- O Tester preenche o quarto campo de resposta com "Porque não?". <br> - O Tester preenche o quarto campo de justificação "Pode acontecer". <br> - O Tester preenche o quinto campo de resposta com "Sim!". <br> - O Tester preenche o quinto campo de justificação "Estou confiant@ que sim!". <br>- O Tester preenche o sexto campo de resposta com "É provável". <br> - O Tester preenche o sexto campo de justificação "Porque já se testou muito isto". <br> - O Tester selecionou a categoria "Teste e Qualidade do Produto". <br> - O Tester selecionou a quinta resposta como correta. <br> O Tester clicou no botão de "Submeter" para submeter o quiz. | - O sistema procedeu com a aceitação do quiz. <br> - O sistema direcionou de novo para a página inicial. <br> - O sistema colocou o quiz na base de dados (onde consegue confirmar-se na lista da página inicial “Criação”) | |--|--|
