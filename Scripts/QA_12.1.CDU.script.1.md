## TESTE DE QUALIDADE#
**USER STORY/FEATURE:**<br>
Como admin quero poder importar quizzes em XML para ser possivel importar quizzes criados noutra PL para a minha aplicação.

**CASO DE USO:**<br>
12.1 - "O ficheiro XML é importado"

**ESPECIFICAÇÃO DO TESTE:**
O tester deve ser capaz de importar quizzes em XML.<br>

**TESTER:**<br>

**Nº DE TESTE:**<br>

**DATA:**<br>



| AÇÃO | ESPERADO | RESULTADO (OK/NOK)|
| :---      |        :--- |        :--- |
|-> O administrador faz login.|-> O sistema apresenta a landing page.||
|-> O administrador carrega no botão de importar quizzes|-> O sistema abre um pop-up para escolha de um ficheiro a importar.||
|-> O administrador escolhe um ficheiro XML.|-> O sistema importa a informação do ficheiro.||

