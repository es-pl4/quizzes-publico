## TESTE DE QUALIDADE

 **USER STORY/FEATURE:** 5-“Enquanto utilizador autenticado quero uma landing page para aceder aos meus status e ações.“

**CASO DE USO:** 5.1 – “Ver Status”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de visualizar todos os dados relativos aos quizzes que ele reveu.

**TESTER:**

**Nº DE TESTE:**

**DATA:**


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| O utilizador autenticado visualiza a secção do site correspondente **aos quizzes aprovados por si**.|  O sistema apresenta os **dados relativos aos quizzes**: Aprovados por mim;|  |
| O utilizador autenticado visualiza a secção do site correspondente **aos quizzes reprovados por si**.|  O sistema apresenta os **dados relativos aos quizzes**: Reprovados por mim;|  | 
| O utilizador autenticado visualiza a secção do site correspondente **aos quizzes por rever no geral**.|  O sistema apresenta os **dados relativos aos quizzes**: Por rever no geral.|  |
| O utilizador autenticado visualiza a secção do site correspondente **ao número total de quizzes revistos por mim**.|  O sistema apresenta os **dados relativos aos quizzes**: Número total de quizzes revistos por mim.|  |
| O utilizador autenticado visualiza a secção do site correspondente **ao número total de quizzes revisto no geral**.|  O sistema apresenta os **dados relativos aos quizzes**: Número referente às revisões (revistos por mim + por rever no geral).|  |
