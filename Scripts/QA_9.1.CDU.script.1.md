## TESTE DE QUALIDADE

**USER STORY/FEATURE:** -“Como resolvedor quero poder seleccionar um teste e resolvê-lo para fazer a minha auto-avaliação e, quem sabe, ir para o top10 do ‘hall of fame’.“
“

**CASO DE USO:** 9.1 – “Visualizar o Top 10 no Hall of Fame”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de visuzalizar corretamente o hall of fame.

**TESTER:**

**Nº DE TESTE:**

**DATA:**


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- | 
| -> O utilizador autenticado visualiza a secção do site correspondente ao **Hall of Fame**.| O sistema apresenta os dados relativos ao **Hall Of Fame**.|  |
| -> Existem dois utilizadores com a mesma classsificação | -> O sistema apresenta os dois utilizadores no mesmo lugar e organizados por ordem alfabética.|  |
| -> Existem dois utilizadores com a mesma classsificação | -> O sistema apresenta o utilizador que se encontra no lugar a seguir aos classificadores empatados e incrementa 1 valor aos mesmo, ou seja, se houverem dois utilizadores em 3º lugar, o utilizador seguinte irá estar em 5º.|  |
| -> O utilizador consegue ver a pontuação dos restantes utilizados. | -> O sistema apresenta a pontuação de todos os utilizados que se encontram dentro do top 10.|  |
| -> O utilizador consegue ver o lugar onde se encontram os restantes utilizados. | -> O sistema apresenta o lugar onde se encontram  todos os utilizados que estão dentro do top 10.|  |
| -> O utilizador consegue ver o nome dos utilizador que aparecem no Hall of Fame. | -> O sistema apresenta o nome todos os utilizados que se encontram dentro do top 10.|  |
