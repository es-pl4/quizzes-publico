## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 1. «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.1 – “Criar Teste” <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve testar se consegue proceder com a submissão do quiz, mesmo que esteja em falta campos de resposta ou a resposta correta por selecionar. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br> 
**DATA:** <br>

| AÇÃO | ESPERADO  | RESULTADO (OK/NOK) |
|--|--|--|
| - O TESTER clicou no botão "Criar Quiz" na página inicial. | - O sistema abriu a página de “Criar Quiz”.| | |--|--|
| - O TESTER preencheu o campo de pergunta com "Não importa a falta de respostas?" <br> - O Tester preenche o primeiro campo de resposta com "Sim". <br> - O Tester preenche o primeiro campo de justificação "Porque é o correto". <br>  - O Tester preenche o segundo campo de resposta com "Talvez". <br> - O Tester preenche o segundo campo de justificação "Porque o código estará direito". <br>- O Tester preenche o terceiro campo de resposta com "Não". <br> - O Tester preenche o terceiro campo de justificação "Porque há algo errado no código". <br>- O Tester preenche o quarto campo de resposta com "Porque não?". <br> - O Tester preenche o quarto campo de justificação "Pode acontecer". <br> - O Tester selecionou a categoria "Teste e Qualidade do Produto". <br> O Tester clicou no botão de "Submeter" para submeter o quiz. | - O sistema impede o quiz de ser submetido, alertando para preencher o quinto campo de resposta e a justificação. | |--|--|
|- O Tester preenche o quinto campo de resposta com "Não!". <br> - O Tester preenche o quinto campo de justificação "Estou confiant@ que não!". <br>- O Tester preenche o sexto campo de resposta com "Não devia". <br> - O Tester preenche o sexto campo de justificação "Porque já se testou muito isto". <br> O Tester clicou no botão de "Submeter" para submeter o quiz. | - O sistema impede o quiz de ser submetido, alertando para selecionar a resposta correta ao quiz. | |--|--|
|- O Tester seleciona a sexta resposta como correta. <br> O Tester clicou no botão de "Submeter" para submeter o quiz.|- O sistema procedeu com a aceitação do quiz. <br> - O sistema direcionou de novo para a página inicial. <br> - O sistema colocou o quiz na base de dados (onde consegue confirmar-se na lista da página inicial “Criação”) |  |--|--|
