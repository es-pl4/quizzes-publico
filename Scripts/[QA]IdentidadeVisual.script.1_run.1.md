## TESTE DE QUALIDADE
**IDENTIDADE VISUAL** - **MANUAL DE NORMAS:**
* 2.2.2. LOGOTIPO - Versão utilizada no website;
* 2.2.3. LOGOTIPO - Versão Horizontal;
* 2.2.4. LOGOTIPO - Versão Vertical.

**ESPECIFICAÇÃO DO TESTE:**
* Verificação da aplicação correta do logotipo no website.

**TESTER:** Tomás Ferreira
**Nº DE TESTE:** 1
**DATA:** 09-12-22


| AÇÃO | ESPERADO | RESUTADO |
|--|--|--|
| O **Tester** deve verificar se a utilização do logotipo esta devidamente aplicada no website segundo o manual de normas | - **Logotipo vertical** apenas na página de login e registo; <br> - **Logotipo horizontal** nas restantes paginas do website. | (aqui escreve-se OK ou NOK para dizer se aconteceu ou não) |
|Login Page|Logo Vertical|OK|
|Landing Page|Logo Horizontal|OK|
|CriarQuiz|Logo Horizontal|OK|
|ReverQuizList|Logo Horizontal|OK|
|ReverQuiz|Logo Horizontal|OK|
|CriarQuiz|Logo Horizontal|OK|
|SelecionarTeste|Logo Horizontal|NOK|
|ResolverTeste|Logo Horizontal|NOK|
|VerSolucao|Logo Horizontal|OK|
|CriarTeste|Logo Horizontal|NOK|
|--|--|
