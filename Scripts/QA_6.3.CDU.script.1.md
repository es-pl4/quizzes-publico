## TESTE DE QUALIDADE

**USER STORY/FEATURE:** 6-“Enquanto utilizador quero fazer login ou registar-me para aceder à Landing Page.“
“

**CASO DE USO:** 6.3 – “Logout”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de realizar logout.

**TESTER:** 

**Nº DE TESTE:** 

**DATA:** 


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| -> O utilizador pressiona o botão de **logout** na página da **landing page**.| O sistema **termina sessão** e envia o utilizador para a página de login!"**.|  |
| -> O utilizador pressiona o botão de **logout** na página de **criar quizzes**.| O sistema **termina sessão** e envia o utilizador para a página de login!"**.|  |
| -> O utilizador pressiona o botão de **logout** na página de **resolver testes**.| O sistema **termina sessão** e envia o utilizador para a página de login!"**.|  |
| -> O utilizador pressiona o botão de **logout** na página de **rever quizzes**.| O sistema **termina sessão** e envia o utilizador para a página de login!"**.|  |
