## TESTE DE QUALIDADE
**IDENTIDADE VISUAL** - **MANUAL DE NORMAS:**
* 3.3. DIMENSÕES MINIMAS

**ESPECIFICAÇÃO DO TESTE:**
* Verificação da aplicação correta do logotipo quanto a sua dimensão mínima no website

**TESTER:** Tomás Ferreira
**Nº DE TESTE:** 1
**DATA:** 09-12-22

| AÇÃO | ESPERADO | RESUTADO |
|--|--|--|
| O **tester** deve verificar se o tamanho mínimo do logotipo está a ser respeitado devidamente segundo o manual de normas. | - O logotipo utilizado **nunca** terá **menos que 80px de largura na versão horizontal** e **menos que 42px de largura na sua versão vertical** | (aqui escreve-se OK ou NOK para dizer se aconteceu ou não) |
|Dimensões Minimas Login Page|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| OK |
|Dimensões Minimas Landing Page|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| OK |
|Dimensões Minimas CriarQuiz|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| OK |
|Dimensões Minimas ReverQuizList|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| OK |
|Dimensões Minimas ReverQuiz|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| OK |
|Dimensões Minimas SelecionarTeste|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| **NOK** |
|Dimensões Minimas ResolverTeste|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| **NOK** |
|Dimensões Minimas VerSolucao|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| OK |
|Dimensões Minimas CriarTeste|Utilização das Dimensões minimas consoante os parâmetros do manual de normas (pag 17)| **NOK** |
|--|--|
