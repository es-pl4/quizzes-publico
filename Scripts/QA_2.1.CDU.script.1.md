## TESTE DE QUALIDADE#
**USER STORY/FEATURE:**
Enquanto revisor, quero rever um quizz para dar seguimento a esse mesmo quizz.

**CASO DE USO:**
2.1 - "Aprovado"<br>

**ESPECIFICAÇÃO DO TESTE:**
O tester deve ser capaz de rever um quiz, clicar no botão de aprovação do quiz e no botão de confirmar.<br>

**TESTER:**<br>

**Nº DE TESTE:**<br>

**DATA:**<br>



| AÇÃO | ESPERADO | RESULTADO (OK/NOK)|
| :---      |        :--- |        :--- |
|-> O revisor abre a página de seleção de quizzes.|-> O sistema apresenta a página com os quizzes disponiveis.||
|-> O revisor revê o quiz escolhido.|-> O sistema apresenta o quiz que o revisor escolheu.| |
|-> O revisor revê o quiz escolhido. <br> -> O revisor clica no botão de aprovação do quiz.|-> O sistema torna ativo o botão de confirmar.||
|-> O revisor clica no botão de confirmar.|-> O sistema apresenta um pop-up de certeza de confirmação.||
|-> O revisor clica no botão de certeza de confirmação..|-> O sistema retira o quiz da pagina de seleção de quizzes associada ao revisor. <br> -> O sistema adiciona uma aprovação ao quiz revisto. <br> -> O sistema volta a mostrar a página de seleção de quizzes. ||

