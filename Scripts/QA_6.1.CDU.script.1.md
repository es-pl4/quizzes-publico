## TESTE DE QUALIDADE

**USER STORY/FEATURE:** 6-“Enquanto utilizador quero fazer login ou registar-me para aceder à Landing Page.“
“

**CASO DE USO:** 6.1 – “Login”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de realizar login.

**TESTER:** 

**Nº DE TESTE:** 

**DATA:** 


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| -> O utilizador preenche o 1º campo com o seu **username**. <br/> -> O utilizador **não preenche** o 2º campo com a sua **password**. <br/> -> O utilizador clica no botão **"Entrar"**.| O sistema apresenta a mensagem **"Preencha todos os campos!"**.|  |
| -> O utilizador **não preenche** o 1º campo com o seu **username**. <br/> -> O utilizador preenche o 2º campo com a sua **password**. <br/>-> O utilizador clica no botão **"Entrar"**.| O sistema apresenta a mensagem **"Preencha todos os campos!"**.|  |
| -> O utilizador preenche o 1º campo um **username** que não está no sistema. <br/> -> O utilizador preenche o 2º campo com a sua **password**. <br/>-> O utilizador clica no botão **"Entrar"**.| O sistema apresenta a mensagem **"Username Inválido!"**.| |
| O utilizador preenche o 1º campo com o seu **username**. <br/> -> O utilizador preenche o 2º campo com uma **password errada**. <br/>-> O utilizador clica no botão **"Entrar"**.| O sistema apresenta a mensagem **"Password Errada!"**.|  |
| -> O utilizador preenche o 1º campo com o seu **username**. <br/> -> O utilizador preenche o 2º campo com a sua **password**. <br/> -> O utilizador clica no botão **"Entrar"**.| O sistema **apresenta a Landing Page**.|  |
