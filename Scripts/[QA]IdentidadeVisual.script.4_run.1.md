## TESTE DE QUALIDADE
**IDENTIDADE VISUAL** - **MANUAL DE NORMAS:**
* 3.1.2. APLICAÇÕES CORRETAS SOBRE FUNDOS.

**ESPECIFICAÇÃO DO TESTE:**
* Verificação da aplicação correta das cores do fundo quanto ao logotipo no website.

**TESTER:** Tomás Ferreira
**Nº DE TESTE:** 1
**DATA:** 09-12-22

| AÇÃO | ESPERADO | RESUTADO |
|--|--|--|
| O **Tester** deve verificar se as cores do fundo onde o logotipo está aplicado estão devidamente aplicadas no website segundo o manual de normas. | - **“HEX: #212121 - Cinza Escuro”** (cor esperada) ou uma outra cor que alcance os parâmetros impostos no manual de normas (pag. 15) como cor do fundo onde o logotipo está aplicado. | (aqui escreve-se OK ou NOK para dizer se aconteceu ou não) |
|Fundos Login Page|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos Landing Page|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos Pagina Criar Teste|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos ReverQuizLista|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos ReverQuiz|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos SelecionarTeste|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos ResolverTeste|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos VerSolucao|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|
|Fundos Criar Teste|Utilização de cores de fundo consoante os parâmetros do manual de normas (pag 15)|OK|

|--|--|
