## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 9. «Como revolvedor, quero poder seleccionar o teste que vou resolver, de forma a realizar a minha auto-avaliação e, quem sabe, ir para o top 10 do 'Hall of Fame'.» <br>
**CASO DE USO:** 9.2 – “Criar Teste” <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve testar se é possível selecionar o teste a resolver, antes de passar à resolução. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO |
|--|--|--|
| - O TESTER, autenticado e, no mínimo. com 3 testes criados, clica no botão "Resolver Teste" | - O sistema abriu a página de “Resolver Teste”.|  | |--|--|
|- O TESTER vê uma lista de testes a serem resolvidos, e que terão sido gerados por outros utilizadores (ou pelo mesmo). O TESTER seleciona o teste que pretende resolver<br> | - O sistema assume a ação e redireciona o TESTER para a página correspondente ao teste que este pretende resolver. |  |--|--|
