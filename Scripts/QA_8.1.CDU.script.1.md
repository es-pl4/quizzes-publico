## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 8. «Como criador de testes, quero poder criar um teste com base em tags existentes, de forma a que a comunidade possa realizar o mesmo e fazer a sua auto-avaliação.» <br>
**CASO DE USO:** 8.1 – “Criar Teste” <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve testar se é possível criar um teste composto por vários quizes dentro das mesmas categorias. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO |
|--|--|--|
| - O TESTER clicou no botão "Resolver Teste" na página inicial. | - O sistema abriu a página de “Resolver Teste”.| OK/NOK | |--|--|
|- O TESTER clica no botão "Criar Teste", no canto inferior esquerdo da página "Selecionar Teste" <br> | - O sistema assume a ação e redireciona o TESTER para a página de criar testes. | OK/NOK |--|--|
|- O TESTER preenche o campo "Nome do Teste" com o nome que pretende dar ao teste<br> | - O sistema assume o nome escolhido | OK/NOK |--|--|
|- O TESTER seleciona o número de categorias que pretende incluir no teste, entre 1 e 3 <br> | - O sistema assume o número de categorias escolhidas, liberando o número de campos de seleção de categorias correspondente ao número escolhido pelo TESTER | OK/NOK |--|--|
|- O TESTER seleciona o número de perguntas que pretende incluir no teste, entre 5, 10 e 15 <br> | - O sistema assume o número de perguntas escolhidas | OK/NOK |--|--|
|- O TESTER seleciona o nome da(s) categoria(s) que pretende incluir no teste, por entre as opções disponíveis <br> | - O sistema assume o nome da(s) categoria(s) escolhida(s). | OK/NOK |--|--|
|- O TESTER clica no botão "Criar Teste" <br> | - O sistema gera um teste com base nas escolhas do TESTER, adicionando-o à base de dados, e redireciona o TESTER para a página "Selecionar Teste". | OK/NOK |--|--|
|- O TESTER está na página "Selecionar Teste" <br> | - O teste criado pelo TESTER aparece disponível para resolver, por entre a lista de testes disponíveis. | OK/NOK |--|--|
