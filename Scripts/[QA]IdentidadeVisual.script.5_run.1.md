## TESTE DE QUALIDADE
**IDENTIDADE VISUAL** - **MANUAL DE NORMAS:**
* 3.2. ÁREA SEGURANÇA 

**ESPECIFICAÇÃO DO TESTE:**
* Verificação da aplicação correta da área de segurança do logotipo no website

**TESTER:** Tomás Ferreira
**Nº DE TESTE:** 1
**DATA:** 09-12-22

| AÇÃO | ESPERADO | RESUTADO |
|--|--|--|
| O **Tester** deve verificar se a área de segurança do logotipo está a ser respeitada devidamente segundo o manual de normas. | - O logotipo utilizado mantem uma margem de, pelo menos, **metade da altura do “E”**, em caixa alta, em “ES”. |  (aqui escreve-se OK ou NOK para dizer se aconteceu ou não) |
|Mergens Login Page|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| OK |
|Mergens Landing Page|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| OK |
|Mergens Criar Quizes|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| OK |
|Mergens ReverQuizLista|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| OK |
|Mergens ReverQuiz|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| OK |
|Mergens SelecionarTeste|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| **NOK** |
|Mergens ResolverTeste|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| **NOK** |
|Mergens CriarTeste|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| **NOK** |
|Mergens VerSolução|Utilização de margens de segurança consoante os parâmetros do manual de normas (pag 16)| OK |
|--|--|--|
