## TESTE DE QUALIDADE
**USER STORY/FEATURE:** «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.2 – “Editar Teste” <br>
**ESPECIFICAÇÃO DO TESTE:** O TESTER deve confirmar se o sistema alerta caso sejam apagados campos de resposta de um quiz existente e não preenchidos devidamente. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:** 

| AÇÃO | ESPERADO  | RESULTADO (OK/NOK) |
|--|--|--|
| - O Tester clica no botão “Reprovado” do quiz pretendido a editar na lista “Criação” na página principal.| - O sistema abriu a página de “Editar Quiz” com os campos já preenchidos do quiz pretendido a editar.| | |--|--|
|- O Tester apaga a resposta 5 e 6 e as suas respetivas justificações. <br> O Tester clica no botão de "Submeter" para submeter o quiz. | - O sistema impede a submissão, obrigando o preenchimento dos campos em falta. | |--|--|
|- O Tester preenche o campo 5 com a resposta e a justificação de novo com os textos que apagou. <br> - O Tester preenche o campo 6 com a resposta e a justificação de novo com os textos que apagou. <br> O Tester clica no botão de "Submeter" para submeter o quiz.| - O sistema aceita a submissão e direcionoume de novo para a página principal. <br> - Na base de dados, a categoria e a resposta correta foram devidamente alteradas.| |--|--|
