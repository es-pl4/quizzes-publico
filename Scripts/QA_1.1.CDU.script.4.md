## TESTE DE QUALIDADE
**USER STORY/FEATURE:** 1. «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.1 – “Criar Teste” | Botão Cancelar <br>
**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de criar um quiz completo, preenchendo todos os campos de pergunta e resposta, a resposta correta e selecionar a categoria da pergunta, clicar no botão "cancelar" e retornar à landing page. <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO |
|--|--|--|
| - O TESTER clicou no botão "Criar Quiz" na página inicial. | - O sistema abriu a página de “Criar Quiz”.|  | |--|--|
| - O TESTER preencheu o campo de pergunta com "Será que consigo voltar atrás?" <br> - O Tester preenche o primeiro campo de resposta com "Sim". <br> - O Tester preenche o primeiro campo de justificação "Porque quero". <br>  - O Tester preenche o segundo campo de resposta com "Talvez". <br> - O Tester preenche o segundo campo de justificação "Porque posso". <br>- O Tester preenche o terceiro campo de resposta com "Não". <br> - O Tester preenche o terceiro campo de justificação "Porque não dá". <br>- O Tester preenche o quarto campo de resposta com "Não sei". <br> - O Tester preenche o quarto campo de justificação "Pode acontecer". <br> - O Tester preenche o quinto campo de resposta com "Sim!". <br> - O Tester preenche o quinto campo de justificação "Porque talvez não funcione". <br>- O Tester preenche o sexto campo de resposta com "É provável". <br> - O Tester preenche o sexto campo de justificação "Porque já se consegui antes". <br> - O Tester selecionou a categoria "Teste e Qualidade do Produto". <br> - O Tester selecionou a segunda resposta como correta. <br> - O Tester clica no botão de "Cancelar" para voltar para a landing page. | - O sistema direcionou o utilizador para a página inicial |  |--|--|
