## TESTE DE QUALIDADE#
**USER STORY/FEATURE:**
Enquanto revisor, quero rever um quizz para dar seguimento a esse mesmo quizz.<br>

**CASO DE USO:**
2.3 - "Não Há Quiz"<br>

**ESPECIFICAÇÃO DO TESTE:**
O tester deve ser capaz de abrir a página de seleção de quizzes e ver se há quizzes disponíveis.<br>
**TESTER:**<br>
**Nº DE TESTE:**<br>
**DATA:**<br>



| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
|-> O revisor abre a página de seleção de quizzes.|-> O sistema apresenta a página com os quizzes disponiveis. <br> -> O sistema informa o revisor de que não existem quizzes para rever.| |


