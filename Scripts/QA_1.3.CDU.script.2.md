## TESTE DE QUALIDADE
**USER STORY/FEATURE:** «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.2 – “Editar Quiz” <br>
**ESPECIFICAÇÃO DO TESTE:** O TESTER deve confirmar se todos os campos são limpos ao clicar no botão “Apagar” na página de “Editar Teste” <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO (OK/NOK) |
|--|--|--|
| - O Tester clica no botão “Reprovado” do quiz pretendido a editar na lista “Criação” na página principal.| - O sistema abriu a página de “Editar Quiz” com os campos já preenchidos do quiz pretendido a editar.| | |--|--|
|- O Tester clica no botão "Apagar". | - O sistema "limpa" todos os campos de pergunta e resposta do quiz. | |--|--|
