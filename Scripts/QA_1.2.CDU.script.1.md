## TESTE DE QUALIDADE
**USER STORY/FEATURE:** «Enquanto autor, quero registar um quiz para ser revisto.» <br>
**CASO DE USO:** 1.2 – “Editar Teste”  <br>
**ESPECIFICAÇÃO DO TESTE:** O TESTER deve confirmar se, de facto, consegue fazer alterações a algumas respostas de um quiz já existente.  <br>
**TESTER:**  <br>
**Nº DE TESTE:**  <br>
**DATA:**

| AÇÃO | ESPERADO  | RESULTADO (OK/NOK) |
|--|--|--|
| - O Tester clica no botão “Reprovado” do quiz pretendido a editar na lista “Criação” na página principal.| - O sistema abriu a página de “Editar Quiz” com os campos já preenchidos do quiz pretendido a editar.| | |--|--|
|- O Tester altera o quarto campo de resposta com "Nova resposta!". <br> - O Tester preenche o quarto campo de justificação "Nova justificação!". <br> - O Tester preenche o quinto campo de resposta com "Outra alteração!". <br> - O Tester preenche o quinto campo de justificação "Mais uma!". <br> O Tester clicou no botão de "Submeter" para submeter o quiz. | - O sistema aceitou a submissão e direcionoume de novo para a página principal. <br> - Na base de dados, as respostas 4 e 5 foram devidamente alteradas.| |--|--|
