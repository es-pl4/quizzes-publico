## TESTE DE QUALIDADE

 **USER STORY/FEATURE:** 5-“Enquanto utilizador autenticado quero uma landing page para aceder aos meus status e ações.“

**CASO DE USO:** 5.1 – “Ver Status”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de visualizar todos os dados relativos ao estado da revisão dos quizzes da sua autoria.

**TESTER:**

**Nº DE TESTE:**

**DATA:**


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| O utilizador autenticado visualiza a secção do site correspondente à **criação de quizzes**.| O utilizador consegue **fazer scroll** e ver todos os quizzes da sua autoria.|  |
| O utilizador autenticado clica no botão **"Filtrar"** criações e escolhe a opção **"todos"**.|  O sistema apresenta **todos os quizzes** criados por ele.|  | 
| O utilizador autenticado clica no botão **"Filtrar"** criações e escolhe a opção **"aprovados"** .|  O sistema apresenta **os quizzes a que o utilizador foi aprovado** .|  |
| O utilizador autenticado clica no botão **"Filtrar"** criações e escolhe a opção **"reprovados"** .|  O sistema apresenta **os quizzes a que o utilizador foi reprovado** .|  |
| O utilizador autenticado clica no botão **"Filtrar"** criações e escolhe a opção **"por rever"** .|  O sistema apresenta **os quizzes criados pelo utilizador que estão por rever** .|  |
