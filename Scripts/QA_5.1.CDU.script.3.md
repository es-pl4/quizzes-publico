## TESTE DE QUALIDADE

 **USER STORY/FEATURE:** 5-“Enquanto utilizador autenticado quero uma landing page para aceder aos meus status e ações.“

**CASO DE USO:** 5.1 – “Ver Status”

**ESPECIFICAÇÃO DO TESTE:** O tester deve ser capaz de visualizar todos os dados relativos à pontuação dos testes que resolveu.
**TESTER:**

**Nº DE TESTE:**

**DATA:**


| AÇÃO | ESPERADO | RESULTADO (OK/NOK) |
| :---      |        :--- |        :--- |
| O utilizador autenticado visualiza a secção do site correspondente à **sua pontuação média nos testes realizados**. |  O sistema apresenta os **dados relativos aos testes**: Pontuação média (de 0 a 10).|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi aprovado**. |  O sistema apresenta os **dados relativos aos testes**: Número de testes aprovados.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes a que foi reprovado**. |  O sistema apresenta os **dados relativos aos testes**: Número de testes reprovados.|  |
| O utilizador autenticado visualiza a secção do site correspondente  **ao número de testes que realizou**. |  O sistema apresenta os **dados relativos aos testes**: Número total de testes .|  |
