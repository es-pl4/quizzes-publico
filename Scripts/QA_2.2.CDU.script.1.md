## TESTE DE QUALIDADE#
**USER STORY/FEATURE:**
Enquanto revisor, quero rever um quizz para dar seguimento a esse mesmo quizz.<br>

**CASO DE USO:**
2.2 - "Recusado"<br>

**ESPECIFICAÇÃO DO TESTE:**
O tester deve ser capaz de reprovar um quiz, preenchendo a justificação de reprovação e clicar no botão de confirmar.<br>

**TESTER:**<br>

**Nº DE TESTE:**<br>

**DATA:**<br>



| AÇÃO | ESPERADO | RESULTADO (OK/NOK)|
| :---      |        :--- |        :--- |
|-> O revisor abre a página de delação de quizzes.|-> O sistema apresenta a página com os quizzes disponíveis.||
|-> O revisor escolhe um quiz.|-> O sistema apresenta o quiz que o revisor escolheu.||
|-> O revisor testa o quiz escolhido. <br> -> O revisor clica no botão de reprovação de quiz.|-> O sistema apresenta uma caixa de introdução de texto para o revisor colocar a justificação de reprovação.||
|-> O revisor preenche a justificação de reprovação com 70 ou mais caracteres.|-> O sistema torna disponível o botão de confirmar.||
|-> O revisor clica no botão confirmar.|-> O sistema apresenta um pop-up de certeza de confirmação. ||
|-> O revisor clica no botão de certeza de confirmação.|-> O sistema retira o quiz da página de seleção de quizzes. <br> -> O sistema adiciona o quiz revisto ao conjunto de quizzes por editar. <br> -> O sistema volta a mostrar a página de seleção de quizzes.||
