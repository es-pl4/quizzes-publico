# TESTAGEM CRUZADA
Olá, outra PL! 
Aqui têm disponiveis alguns scripts de teste para a testagem das features implementadas no nosso projeto "QuizzES". 
No final do ficheiro, também temos disponivel o template de script para testagem se precisarem usar. 

| USER STORY | CASO DE USO | SCRIPT |
|--|--|--|
| User Story 1 | CASO DE USO 1.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.1.CDU.script.1.md <br> SCRIPT 2: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.1.CDU.script.2.V2.md <br> SCRIPT 3: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.1.CDU.script.3.md <br> SCRIPT 4: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.1.CDU.script.4.md <br> SCRIPT 5: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.1.CDU.script.5.md 
|User Story 1| CASO DE USO 1.2 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.2.CDU.script.1.md <br> SCRIPT 2: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.2.CDU.script.2.md <br> SCRIPT 3: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.2.CDU.script.3.md <br> SCRIPT 4: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.2.CDU.script.4.md <br> SCRIPT 5: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.2.CDU.script.5.md 
|User Story 1| CASO DE USO 1.3 |SCRIPT 1:https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.3.CDU.script.1.md <br> SCRIPT 2:https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_1.3.CDU.script.2.V2.md 
| User Story 2 | CASO DE USO 2.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/0f2e3b3da4de4ced2b51b00eb808df11e2c7d54a/Scripts/QA_2.1.CDU.script.1.md 
| User Story 2| CASO DE USO 2.2 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_2.2.CDU.script.1.md 
| User Story 2 | CASO DE USO 2.3 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_2.3.CDU.script.1.md
| User Story 3 | CASO DE USO 3.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_3.1.CDU.script.1.md <br> SCRIPT 2: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_3.1.CDU.script.2.md 
| User Story 4 | CASO DE USO 4.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_4.1.CDU.script.1.md |
| User Story 5: | CASO DE USO 5.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_5.1.CDU.script.1.md <br> SCRIPT 2: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_5.1.CDU.script.2.md <br> SCRIPT 3: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_5.1.CDU.script.3.md  <br> SCRIPT 4: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_5.1.CDU.script.4.md  
| User Story 5 | CASO DE USO 5.2 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_5.2.CDU.script.1.md 
| User Story 6 | CASO DE USO 6.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_6.1.CDU.script.1.md  
| User Story 6 | CASO DE USO 6.2 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_6.2.CDU.script.1.md 
| User Story 6 | CASO DE USO 6.3 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_6.3.CDU.script.1.md 
| User Story 8 | CASO DE USO 8.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_8.1.CDU.script.1.md 
| User Story 9 | CASO DE USO 9.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_9.1.CDU.script.1.md 
| User Story 9 | CASO DE USO 9.2 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_9.2.CDU.script.1.md 
| User Story 11 | CASO DE USO 11.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_11.1.CDU.script.1.md  
| User Story 12 | CASO DE USO 12.1 | SCRIPT 1: https://gitlab.com/es-pl4/quizzes-publico/-/blob/main/Scripts/QA_12.1.CDU.script.1.md 



---- começa aqui ----
## TESTE DE QUALIDADE
**USER STORY/FEATURE:** <br>
**CASO DE USO:** <br>
**ESPECIFICAÇÃO DO TESTE:** <br>
**TESTER:** <br>
**Nº DE TESTE:** <br>
**DATA:** <br>
| AÇÃO | ESPERADO | RESUTADO |
|--|--|--|
| (aqui escreve-se o que o tester deve fazer) | (aqui escreve-se a resposta do sistema esperada) | (aqui escreve-se OK ou NOK para dizer se aconteceu ou não||--|--|

---- acaba aqui -----

