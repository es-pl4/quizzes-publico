# **REQUISITOS** #

Pasta destinada aos ficheiros relativos a requisitos: User Stories, Casos de Uso, Mockups e Testes.


### **NOMENCLATURAS** ###

**User Story:**

[REQ]NúmeroDaStory.UserStory.md

Ex: [REQ]2.UserStory.md


**Caso de Uso:**

[REQ]NúmeroDaStory.NúmeroDoCaso.CasoDeUso.md

Ex: [REQ]2.1.CasoDeUso.md


**Testes:**

[REQ]NúmeroDaStory.NúmeroDoCaso.CasoDeUso.Teste.md

Ex: [REQ]2.1.CasoDeUso.Teste.md


**Mockups:**

[REQ]NúmeroDaStory.Mockup.png

Ex: [REQ]2.Mockup.png

### **Template User Story** ###

# User Story (nº da user story)
...


### **Template Casos de Uso** ###

## (nome do caso de uso)

# Primary Actor:
...

# Preconditions:
...

# Success Guarantee:
...

# Main Success Scenario:
1. ...
2. ...
3. ...
4. ...
5. ...

# Extensions:

- 1a.  ...
- 1b. ...

# Exception:
- 1a. ...
- 1b. ...
- 2a. ...

