# **CASO DE USO** - _Criar teste_
### **Primary Actor:** Utilizador Autenticado  
### **Preconditions:**
* O utilizador está autenticado no sistema.
* O utilizador criou pelo menos 1 teste.
* O utilizador já reveu pelo menos 3 testes.
* O utilizador selecionou "Criar Teste" na homepage.
### **Minimal Guarantee:**
* O programa dá ao utilizador uma mensagem de erro.
* A mensagem de erro pode indicar que o campo de preenchimento de nome não foi devidamente carregado.
* A mensagem de erro pode indicar que algum dos campos de seleção não foi devidamente carregado.
* Ao selecionar o número de perguntas, os campos de seleção com os nomes das categorias podem não ser devidamente carregados.
### **Success Guarantee:**
* O programa carrega corretamente a lista de testes criados e permite que o utilizador escolha aquele que quer resolver.
### **Main Success Scenario:**
###### 1. O programa apresenta corretamente os campos a serem preenchidos pelo utilizador.
###### 2. O utilizador preenche o campo com o nome que pretende dar ao teste.
###### 3. O utilizador seleciona as opções nos campos de seleção (número de perguntas, número de categorias, categorias).
###### 4. O utilizador clica em "Criar Teste", o teste é adicionado à base de dados e o utilizador é redirecionado para a landing page.

### **Extensions:**
###### 4a. O utilizador não preenche todos os campos.
###### 4a1. O utilizador não é redirecionado para a landing page.
###### 5a. O utilizador retrocede.
###### 5a1. O utilizador é redirecionado para a landing page, sem que o teste seja adicionado à base de dados.
