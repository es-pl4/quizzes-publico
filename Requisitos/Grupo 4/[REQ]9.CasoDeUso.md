# **CASO DE USO** - _Selecionar teste_
### **Primary Actor:** Utilizador Autenticado  
### **Preconditions:**
* O utilizador está autenticado no sistema.
* O utilizador criou pelo menos 1 teste.
* O utilizador já reveu pelo menos 3 testes.
* O utilizador selecionou "Resolver Quiz" na homepage.
### **Minimal Guarantee:**
* O programa dá ao utilizador uma mensagem de erro.
* A mensagem de erro pode indicar que os testes não foram devidamente carregados e não permitir fazer a seleção.
* Ao selecionar, o utilizador pode não ser redirecionado para a página de resolução.
* Ao selecionar, o utilizador pode ser redirecionado erradamente, indo até à página de resolução de um teste que não foi o selecionado.
### **Success Guarantee:**
* O programa carrega corretamente a lista de testes criados e permite que o utilizador escolha aquele que quer resolver.
### **Main Success Scenario:**
###### 1. O programa apresenta a lista de testes possíveis a resolver.
###### 2. O utilizador seleciona o teste que pretende resolver.
###### 3. O utilizador é redirecionado para a página de resolução do teste.

### **Extensions:**
###### 4a. O utilizador não seleciona o teste.
###### 4a1. O utilizador não é redirecionado para a página de resolução.
###### 5a. O utilizador retrocede.
###### 5a1. O utilizador é redirecionado para a landing page.
