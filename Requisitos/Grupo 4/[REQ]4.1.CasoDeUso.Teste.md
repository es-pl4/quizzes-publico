# **Testagem da página de Resolver Quiz** 

### **Preconditions:**

|  Teste     | Resultado   | Comentário |
| :---      |        :--- | :---        |
|- Para resolver o quiz, é feita a verificação de que o utilizador está autenticado no sistema.  |Reprovado |Por implementar   |
|- Para resolver o quiz, é feita a verificação de que o utilizador já criou um quiz.  |Reprovado |Por implementar   |
|- Para resolver o quiz, é feita a verificação de que o utilizador já reveu pelo menos três quizzes.  |Reprovado |Por implementar |


### **Main Success Scenario:**
|  Teste     | Resultado   | Comentário |
| :---       |:---          | :---        |
|- A página carrega sem dar erro.  |Aprovado |A página carrega sem qualquer erro.|
|- O programa apresenta o quiz a responder. |Aprovado |O programa apresenta o quiz com base no ficheiro JSON. |
|- As perguntas aparecem corretamente listadas e numeradas.|Aprovado |            |
|- As opções aparecem corretamente listadas.  |Aprovado |            |
|- O utilizador consegue responder às perguntas que achar corretas. |Aprovado |O utilizador consegue selecionar a checkbox relativa á resposta que ele considerar correta|
|- O utilizador consegue submeter o quiz para ser avaliado.|||
|- O programa verifica se ficou alguma pergunta por responder,notificando o utilizador do mesmo.|Aprovado|O programa verifica se ficou alguma pergunta por responder, notificando o utilizador de cada pergunta que ficou por responder |
|- O programa verifica se alguma pergunta tem mais do que uma resposta e notifica o utilizador do mesmo.|Aprovado|O programa verifica se alguma pergunta foi respondida mais  do que uma vez e notifica o utilizador do mesmo. |
|- O botão "homepage" redireciona o utilizador para a pagina principal.|Reprovado| Por implementar|
