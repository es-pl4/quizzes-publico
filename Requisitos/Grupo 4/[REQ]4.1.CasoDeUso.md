# **CASO DE USO** - _Resolver o teste_
### **Primary Actor:** Utilizador Autenticado  
### **Preconditions:**
* O utilizador está autenticado no sistema.
* O utilizador criou pelo menos 1 teste.
* O utilizador já reveu pelo menos 3 testes.
* O utilizador selecionou "Resolver Quiz" na homepage.
### **Minimal Guarantee:**
* O programa dá ao utilizador uma mensagem de erro.
* A mensagem de erro pode indicar que existem perguntas por responder, erros internos no sistema, entre outros erros que possam vir a acontecer.
### **Success Guarantee:**
* O programa define o teste como resolvido e pronto a ser avaliado.
### **Main Success Scenario:**
###### 1. O programa apresenta o teste a responder.
###### 2. O utilizador responde às perguntas selecionando as respostas que acha estarem corretas.
###### 3. O utilizador submete o teste para avaliação.
###### 4. O programa verifica se alguma pergunta ficou por responder.
###### 5. O programa envia o teste para verificar a solução.
### **Extensions:**
###### 4a. O utilizador não responde a uma ou mais perguntas.
###### 4a1. Ao submeter, o programa verifica se é obrigatório responder às perguntas não preenchidas.
###### 4a2. Caso seja obrigatório, um aviso informa o utilizador que deixou perguntas por responder.
###### 4a3. Caso não seja obrigatório, o prgrama envia o teste para verificação normalmente.
