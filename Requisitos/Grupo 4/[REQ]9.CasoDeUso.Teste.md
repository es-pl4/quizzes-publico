# **Testagem da página de Resolver Quiz** 

### **Preconditions:**

|  Teste     | Resultado   | Comentário |
| :---      |        :--- | :---        |
|- Para resolver o quiz, é feita a verificação de que o utilizador está autenticado no sistema.  |Aprovado |   |
|- Para resolver o quiz, é feita a verificação de que o utilizador já criou um quiz.  | |   |
|- Para resolver o quiz, é feita a verificação de que o utilizador já reveu pelo menos três quizzes.  | | |
|- Para resolver o quiz, é feita a verificação de que o utilizador selecionou "Resolver Quiz" na homepage.  |Aprovado | |


### **Main Success Scenario:**
|  Teste     | Resultado   | Comentário |
| :---       |:---          | :---        |
|- O programa carrega corretamente a lista de testes criados e permite que o utilizador escolha aquele que quer resolver.  |Aprovado |O utilizador pode escolher o teste pretendido da lista.|
|- A página carrega sem dar erro.  |Aprovado |A página carrega sem qualquer erro.|
|- O programa apresenta a lista de testes possíveis a resolver.  |Aprovado |São apresentadas variadas opções de testes.|
|- O utilizador seleciona o teste que pretende resolver. |Aprovado| |
|- O utilizador é redirecionado para a página de resolução do teste.|Aprovado | Clicar num teste redireciona para a resolução do mesmo.|
