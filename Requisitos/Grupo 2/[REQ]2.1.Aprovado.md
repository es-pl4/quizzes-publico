## Caso de Uso #1: O quiz é aceite

**Primary Actor:**
Revisor

**Level:**
Kite

**Stakeholders and Interests:** 
Revisor

**Preconditions:**
O revisor está autenticado e acedeu à página de revisão de quizzes.

**Success Guarantee:**
O quizz é aceite por três revisores e o sistema vai tirar o quizz da página de revisão, sendo que este passa para a fase seguinte de resolução. 

**Main Success Scenario:**
1. O revisor recebe quizzes para rever.
2. O revisor revê o quizz.
3. O revisor clica “check” e automaticamente o botão “confirmar” fica ativo.
4. O revisor clica no botão “confirmar”.
5. O sistema faz aparecer um pop-up de certeza de confirmação.
8. O sistema retira o quiz da página de revisão associada ao revisor.
9. O sistema adiciona uma aprovação ao quiz.
10. O sistema volta a mostrar a página que contém os quizzes a rever.

**Extensions:**
- 1a. O revisor não aceita o quiz.
- 2a. O revisor não revê o quiz.


**Exceptions:** 
- 4a. O sistema não deixa confirmar a revisão.
- 8a. O sistema não retira o quiz após ser revisto. 
- 9a. O sistema não guarda a revisão.
