**[CASO DE USO # 11.1: O ficheiro XML é exportado]**

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O administrador faz o login. |- O sistema apresenta a landing page.|
|- O administrador carrega no botão de exportar os quizzes. | - O sistema começa o donwnload do ficheiro XML com a informação dos quizzes.|


**Exceptions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O administrador carrega no botão de exportar os quizzes. | - O sistema não começa o download do ficheiro no dispositivo do administrador.|
