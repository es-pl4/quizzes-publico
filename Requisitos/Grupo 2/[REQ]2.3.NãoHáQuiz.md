## Caso de uso #3 — Não há quizz para rever

**Primary Actor:**
Revisor

**Level:**
Kite

**Stakeholders and Interests:**
Revisor

**Preconditions:** 
O revisor está autenticado.

**Success Guarantee:** 
O sistema carrega corretamente a página de rever quizzes e informa o revisor que não tem nenhum quiz para rever. 

**Main Success Scenario:** 
1. O revisor entra na página “rever quiz”.
2. O sistema exibe a “página inicial” do rever quiz.
3. O sistema informa o revisor que não existem quizzes para rever. 

**Extensions:** 
- 1a. O revisor não entra na página “rever quiz”.

**Exceptions:**
- 2a. O sistema não exibe a página inicial do rever quiz.
- 3a. O sistema avisa o revisor que tem quizzes por rever,mas não tem nenhum.
- 3b. O sistema não informa o revisor que não existem quizzes para rever.
