**[CASO DE USO # 12.1: O ficheiro XML é importado]**

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O administrador faz login. |- O sistema apresenta a landing page.|
|- O administrador carrega no botão de importar quizzes |- O sistema abre um pop-up para escolha de um ficheiro a importar.|
|- O administrador escolhe um ficheiro XML. |- O sistema importa a informação do ficheiro. |


**Extensions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O administrador escolhe um ficheiro que não é do formato XML. |- O sistema exibe um alerta a informar que o ficheiro selecionado não tem o formato correto |

**Exceptions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O administrador carrega no botão de importar quizzes |- O sistema não abre o pop-up necessário para a escolha de ficheiro a importar.|
|- O administrador escolhe um ficheiro XML. |- O sistema falha na importação do ficheiro. |
