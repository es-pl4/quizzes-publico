**[CASO DE USO # 2: O quiz é recusado]**

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor abre a página de seleção de quizzes.|- O sistema apresenta a página com os quizzes disponiveis.|
|- O revisor escolhe um quiz.|- O sistema apresenta o quiz que o revisor escolheu. |
|- O revisor revê o quiz escolhido.<br/>- O revisor clica no botão de reprovação do quiz.|- O sistema apresenta uma caixa de introdução de texto para o revisor colocar a justificação da reprovação. |
|- O revisor preenche a justificação de reprovação com 70 ou mais caracteres.|- O sistema torna disponivel o botão de confirmar.
|- O revisor clica no botão de confirmar.|- O sistema apresenta um pop-up de certeza de confirmação. |
|- O revisor clica no botão de certeza de confirmação.|- O sistema retira o quiz da pagina de seleção de quizzes. <br/>- O sistema adiciona o quiz revisto ao conjunto de quizzes por rever. <br/>- O sistema volta a mostrar a página de seleção de quizzes. |


**Extensions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor não escolhe um quiz |- O sistema não abre nenhum quiz para ser revisto|
|- O revisor não revê o quiz. <br/>- O revisor não clica no botão de rejeição de quiz.|- O sistema não apresenta a caixa de introdução de tezto para  a justificação de reprovação|
|- O revisor não preenche devidamente a justificação de rejeição do quiz com 70 ou mais caracteres|- O sistema não torna ativo o botão de certeza de confirmação |
|- O revisor aborta o processo a qualquer altura |- O sistema regressa à página de seleção de quizzes|

**Exceptions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor revê o quiz escolhido <br/>- O revisor clica no botão de reprovação do quiz. |- O sistema não apresenta a caixa de introdução de texto.|
|- O revisor clica no botão de confirmar.|- O sistema não apresenta o pop-up necessário para a certeza de confirmação. |
|- O revisor clica no botão de certeza de confirmação.|- O sistema mantém o quiz na pagina de seleção de quizzes. <br/>- O sistema não adiciona o quiz revisto ao conjunto de quizzes por rever. <br/>- O sistema não regressa à página de seleção de quizzes. |
