**[CASO DE USO # 1: O quiz é aceite]**

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor abre a página de seleção de quizzes.|- O sistema apresenta a página com os quizzes disponiveis.|
|- O revisor escolhe um quiz.|- O sistema apresenta o quiz que o revisor escolheu. |
|- O revisor revê o quiz escolhido.<br/>- O revisor clica no botão de aprovação do quiz.|- O sistema torna ativo o botão de confirmar. |
|- O revisor clica no botão de confirmar.|- O sistema apresenta um pop-up de certeza de confirmação. |
|- O revisor clica no botão de certeza de confirmação.|- O sistema retira o quiz da pagina de seleção de quizzes associada ao revisor. <br/>- O sistema adiciona uma aprovação ao quiz revisto. <br/>- O sistema volta a mostrar a página de seleção de quizzes. |


**Extensions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor não escolhe um quiz |- O sistema não abre nenhum quiz para ser revisto|
|- O revisor não revê o quiz. <br/>- O revisor não clica no botão de aprovação de quiz. |- O sistema não torna ativo o botão de certeza de confirmação |
|- O revisor aborta o processo a qualquer altura |- O sistema regressa à página de seleção de quizzes|

**Exceptions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor revê o quiz escolhido <br/>- O revisor clica no botão de aprovação do quiz. |-O sistema não torna o botão de confirmar ativo|
|- O revisor clica no botão de confirmar.|- O sistema não apresenta o pop-up necessário para a certeza de confirmação. |
|- O revisor clica no botão de certeza de confirmação.|- O sistema mantém o quiz na pagina de seleção de quizzes associada ao revisor. <br/>- O sistema não regista a adicão de uma aprovação ao quiz revisto. <br/>- O sistema não regressa à página de seleção de quizzes. |
