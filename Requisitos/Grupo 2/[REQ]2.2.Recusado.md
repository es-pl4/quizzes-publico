## Caso de uso #2 — O quiz é recusado

**Primary Actor:** 
Revisor  

**Level:**
Kite

**Stakeholders and Interests:**
Revisor

**Preconditions:** 
O utilizador está autenticado e acedeu à página de revisão do quiz.

**Success Guarantee:** 
O sistema recebe corretamente a avaliação e a justificação do revisor, adicionando o quiz respectivo ao conjunto de quizzes por refazer. 

**Main Success Scenario:**
1. O revisor seleciona o botão de reprovação.
2. O revisor preenche a justificação da reprovação, usando um mínimo de 70 caracteres.
3. O sistema verifica que os campos estão preenchidos corretamente e ativa o botão confirmar. 
4. O revisor submete a sua avaliação.  
5. O sistema faz aparecer um pop-up de certeza de confirmação.
6. O sistema retira o quiz da página de revisão.
7. O sistema adiciona o quiz ao conjunto de quizzes por refazer (mandar para trás). 
8. O sistema volta a mostrar a página que contém os quizzes a rever. 

**Extensions:**
- 1a. O revisor seleciona o botão de aprovar o quiz. 
- 4a. O revisor abandona a revisão.
- 5a. O revisor rejeita o pop-up de confirmação.

**Exceptions:**
- 2a. O revisor não preenche a justificação de reprovação com 70 ou mais caracteres.
- 3a. O sistema ativa o botão de confirmar sem os campos devidamente preenchidos.
- 3b. O sistema não ativa o botão de confirmação mesmo com os campos devidamente preenchidos.
- 5b. O sistema não faz o pop-up de certeza de confirmação aparecer.
- 5c. O sistema produz o pop-up de certeza de confirmação com erros de funcionamento.
- 6a. O sistema não faz a submissão corretamente.
- 7a. O sistema mantém o quiz na página de revisão.
- 8a. O sistema não consegue adicionar o quiz ao conjunto de quizzes por refazer.
- 8b. O sistema coloca o quiz rejeitado na página errada (como a página de revisão).
