**[CASO DE USO # 3: Não há quiz para rever.]**

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor abre a página de seleção de quizzes.|- O sistema apresenta a página com os quizzes disponiveis.<br/>- O sistema informa o revisor de que não existem quizzes para rever.|


**Extensions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor não entra na página de seleção de quizzes. |- O sistema não apresenta a página com os quizzes disponiveis|

**Exceptions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O revisor abre a página de seleção de quizzes.|- O sistema não informa o revisor de que não existem quizzes por rever, embora não existam.|
