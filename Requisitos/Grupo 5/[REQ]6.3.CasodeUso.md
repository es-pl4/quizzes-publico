## Logout

# Primary Actor:
Utilizador.

# Preconditions:
O utilizador está auntenticado.

# Success Guarantee:
O sistema mostra ao utilizador o botão de logout e ao ser precionado termina a sessão e envia o utilizador para a página de login.

# Main Success Scenario:
1. O utilizador preciona o botão de logout.
2. O sistema termina a sessão.

# Extensions:
- 2a. O utilizador é direcionado para a página de login.

# Exception:
