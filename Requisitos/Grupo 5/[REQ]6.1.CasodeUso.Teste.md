[CASO DE USO 6.1- Login]

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador preenche o 1º campo com o seu **username**. <br/> -> O utilizador preenche o 2º campo com a sua **password**. <br/>-> O utilizador clica no botão **"Entrar"**.|-> O sistema **apresenta a Landing Page**. |


**Extensions:**
|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador preenche o 1º campo com o seu **nome de utilizador**.<br/> -> O utilizador preenche o 2º campo com a sua **palavra-passe**.<br/> -> O utilizador seleciona a opção **"Entrar"**.|-> O sistema **rejeita o login** ao detetar que o **username** está errado, mostrando a mensagem "Username Inválido!".<br/> -> O sistema **rejeita o login** ao detetar que a **password** está errada, mostrando a mensagem "Password Errada!".  <br/> -> O sistema **rejeita o login** ao detetar que os campos não estão todos preenchidos, mostrando a mensagem "Preencha todos os campos!".
