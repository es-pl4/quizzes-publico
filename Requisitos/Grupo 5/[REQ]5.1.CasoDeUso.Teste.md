[CASO DE USO 5.1- Ver Status]

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador autenticado visualiza a secção do site correspondente à **criação de quizzes**. <br/>-> O utilizador autenticado clica no botão **"Filtrar"** criações e escolhe a opção **"todos", "aprovado", "reprovado" ou "por rever"** . | -> O sistema apresenta os **quizzes a que o utilizador foi "aprovado", "reprovado", "por rever" ou "todos"**. <br/>->O utilizador consegue **fazer scroll** e ver todos os quizzes conforme a seleção que foi feita.|
|-> O utilizador autenticado visualiza a secção do site correspondente à **revisão de quizzes**. |-> O sistema apresenta os **dados relativos aos quizzes**:<br/>  -Aprovados por mim;<br/> -Reprovados por mim;<br/>  -Por Rever no geral;<br/>  -Número total de quizzes revistos por mim;<br/> -Número referente às revisões (revistos por mim + por rever no geral).|
|<br/>-> O utilizador autenticado visualiza a secção do site correspondente à **sua performance**. |-> O sistema apresenta os **dados relativos aos testes**:<br/>-Pontuação média (de 0 a 10);<br/>-Número de testes aprovados;<br/>-Número de testes reprovados;<br/>-Número total de testes; <br/>-Média que obteve em cada categoria. |
|<br>-> O utilizador autenticado seleciona o botão **"Logout"**.|-> O sistema permite ao utilizador **terminar sessão**.|


**Extensions:**
|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador autenticado visualiza a secção do site correspondente à **criação de quizzes** <br/>-> O utilizador autenticado clica no botão **"Filtrar"** criações e escolhe a opção **"todos", "aprovado", "reprovado" ou "por rever"** . | -> O sistema **não apresenta** os dados relativos às autorias do utilizador. |
|-> O utilizador autenticado visualiza a secção do site correspondente à **revisão de quizzes**. |-> O sistema **não apresenta dados** relativos aos quizzes aprovados e reprovados pelo utilizador, **criando uma notificação** com a seguinte mensagem "Ainda não existem quizzes revistos!".<br/> |
|-> O utilizador autenticado visualiza a secção do site correspondente à **sua performance**. |-> O sistema **não apresenta a média** do utilizador,dá erro porque nao tem valores inseridos.<br/>-> O sistema **não apresenta o gráfico de competências** do utilizador, a percentagem que o utilizador obteve em cada categoria não aparece.<br/>-> O sistema **não tem dados relativos aos testes realizados** pelo utilizador.|
|-> O utilizador autenticado seleciona o botão **"Logout"**. |-> O sistema não permite ao utilizador **terminar sessão**.|
