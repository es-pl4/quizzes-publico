[CASO DE USO 6.2- Registar]

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador preenche o 1º campo com o seu **nome**.<br/> -> O utilizador preenche o 2º campo com o seu **e-mail**.<br/> -> O utilizador preenche o 3º campo com o seu **username**.<br/> -> O utilizador preenche o 4º campo com a sua **password**.<br/> -> O utilizador preenche o 5º campo com a sua **password** para confirmar com a anterior.<br/>-> O utilizador clica no botão **"Registar"**.|-> O sistema operativo **apresenta a Página do Login** e apresenta a mensagem "Utilizador registado com sucesso". |


**Extensions:**
|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador preenche o 1º campo com o seu **nome**.<br/> -> O utilizador preenche o 2º campo com o seu **e-mail**.<br/> -> O utilizador preenche o 3º campo com o seu **username**.<br/> -> O utilizador preenche o 4º campo com a sua **password**.<br/> -> O utilizador preenche o 5º campo com a sua **password** para confirmar com a anterior.<br/> -> O utilizador seleciona a opção **"Registar"**.|-> O sistema **rejeita o registo** ao detetar que o username está repetido e mostra a mensagem "Username já registado!".<br/>-> O sistema **rejeita o registo** ao detetar que o e-mail está repetido e mostra a mensagem "E-mail já registado!".<br/>-> O sistema **rejeita o registo** ao detetar que as palavras-passes não coincidem e mostra a mensagem "Passwords diferentes!".<br/>-> O sistema **rejeita o login** ao detetar que os campos não estão todos preenchidos, mostrando a mensagem "Preencha todos os campos!".<br/>-<O sistema **rejeita o login** com os dados que foram criados na página de registar|
