## Login

# Primary Actor:
Utilizador.

# Preconditions:
O utilizador está na página do Login.

# Success Guarantee:
O sistema mostra ao utilizador os campos que tem de preencher: "nome de utilizador" e "password".

# Main Success Scenario:
1. O utilizador preenche os dados do 1º campo corretamente.
2. O utilizador preenche os dados do 2º campo corretamente.
3. O utilizador faz login.

# Extensions:
- 3a. O utilizador é direcionado para a página da Landign Page.

# Exception:
- 1a. O utilizador não preenche os dados do 1º campo corretamente.
- 1b. O utilizador não preenche o 1º campo.
- 2a. O utilizador não preenche os dados do 2º campo corretamente.
- 2b. O utilizador não preenche o 2º campo.
- 3a. O utilizador não faz login.
