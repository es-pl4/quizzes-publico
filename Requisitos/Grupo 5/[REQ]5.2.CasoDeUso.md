## Ações

# Primary Actor:
Utilizador autenticado

# Preconditions:
O utilizador está autenticado.
O utilizador autenticado está na Landing Page.

# Success Guarantee:
O utilizador está autenticado e o sistema apresenta as várias ações que pode realizar.

# Main Success Scenario:
1. O utilizador pode criar quizzes.
2. O utilizador pode rever quizzes.
3. O utilizador pode resolver testes.
4. O utilizador pode terminar sessão.

# Extensions:
- 1a. O utilizador clica no botão "Criar Quizz" onde será redirecionado para a página de criação de quizzes.
- 2a. O utilizador clica no botão "Rever Quizz" onde será redirecionado para a página de revisão de quizzes.
- 3a. O utilizador clica no botão "Resolver Teste" onde será redirecionado para a página de resolução de testes.
- 4a. O utilizador clica no botão referente ao seu perfil de usuário onde terá outro botão para terminar sessão.

# Exception:
- 1a. O sistema não redireciona o utilizador ao clicar no botão.
- 2a. O sistema não redireciona o utilizador ao clicar no botão.
- 2b. O utilizador só pode rever quizzes se existirem quizzes por rever, caso esta condição não seja cumprida, o botão de rever quizzes está bloqueado.
- 3a. O sistema não redireciona o utilizador ao clicar no botão.
- 3b. Mesmo tendo os requisitos para criar testes completos, o sistema mantém o botão bloqueado.
- 3c. O utilizador só pode resolver testes se já tiver criado 1 quizz e revisto 3, ou seja, caso estas condições não sejam cumpridas, o botão de resolver testes está bloqueado.
