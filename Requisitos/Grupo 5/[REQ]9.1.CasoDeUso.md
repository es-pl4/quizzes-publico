## Visualizar o Top 10 no Hall of Fame

# Primary Actor:
Utilizador Autenticado

# Preconditions:
O utilizador está autenticado.
O utilizar autenticado está na landing page.

# Success Guarantee:
O sistema mostra ao utilizador o ranking dos utilizadores, de forma ascendente, do maior número de Quizzes certos para o menor. 

# Main Success Scenario:
1. O utilizador autenticado visualiza a secção do site correspondente ao Hall of Fame.
2. O utilizador termina sessão.

# Extensions:

# Exception:
- 1a. O sistema não apresenta os dados relativos ao número de QuizzEs certos por cada utilizador.
- 1b. O sistema apresenta dois utilizadores com a mesma classificação.
- 1c. O sistema não tem 10 valores sobre utilizadores para mostrar, por isso há espaços em branco.
