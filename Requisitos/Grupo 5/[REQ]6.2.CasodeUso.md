## Register

# Primary Actor:
Utilizador.

# Preconditions:
O utilizador está na página de registar.

# Success Guarantee:
O sistema mostra ao utilizador os campos que tem de preencher: "nome", "email", "username", "password" e "confirmar password".

# Main Success Scenario:
1. O utilizador preenche os dados do 1º campo corretamente.
2. O utilizador preenche os dados do 2º campo corretamente.
3. O utilizador preenche os dados do 3º campo corretamente.
4. O utilizador preenche os dados do 4º campo corretamente.
5. O utilizador preenche os dados do 5º campo corretamente.
6. O utilizador é registado.

# Extensions:
- 6a. O utilizador é direcionado para a página de login.

# Exception:
- 1a. O utilizador não preenche os dados do 1º campo corretamente.
- 1b. O utilizador não preenche o 1º campo.
- 2a. O utilizador não preenche os dados do 2º campo corretamente.
- 2b. O utilizador não preenche o 2º campo.
- 3a. O utilizador não preenche os dados do 3º campo corretamente.
- 3b. O utilizador não preenche o 3º campo.
- 4a. O utilizador não preenche os dados do 4º campo corretamente.
- 4b. O utilizador não preenche o 4º campo.
- 5a. O utilizador não preenche os dados do 5º campo corretamente.
- 5b. O utilizador não preenche o 5º campo.
- 6a. O utilizador não é registado.
