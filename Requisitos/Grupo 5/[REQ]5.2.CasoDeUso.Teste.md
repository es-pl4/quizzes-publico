[CASO DE USO 5.2- Ações]

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador autenticado clica no botão **"Criar Quiz"**. | -> O utilizador autenticado tem acesso à **página de outro grupo**. |
|-> O utilizador autenticado clica no botão **"Rever Quiz"**. |-> O utilizador autenticado tem acesso à **página de outro grupo**.|
|->O utilizador autenticado clica no botão **"Resolver Teste"**. |-> O utilizador autenticado tem acesso à **página de outro grupo**. |
|O utilizador autenticado seleciona o botão **"Logout"**.|-> O sistema permite ao utilizador **terminar sessão**.|


**Extensions:**
|  Ação      | Resultado   |
| :---      |        :--- |
|-> O utilizador autenticado clica no botão **"Criar Quiz"**.  | -> O sistema **não guarda** os quizzes que não estão finalizados.<br/>-> O utilizador autenticado tem a opção de acabar um quiz iniciado por si anteriormente antes de criar um quiz novo e, ainda, aparece uma **notificação** quando clica no botão "Criar Quiz" a dizer "Ainda tem x quizzes por terminar!". |
|-> O utilizador autenticado clica no botão **"Rever Quiz"**. |-> O botão relativo ao rever quizzes está **bloqueado** mesmo havendo quizzes por rever.<br/>-> O botão relativo ao rever quizzes está **disponível** mesmo não havendo quizzes por rever. |
|-> O utilizador autenticado clica no botão **"Resolver Teste"**. |->O botão não aparece;<br/>->(Por Implementar) O botão **"Resolver Teste"** não está bloqueado mesmo se o utilizador já tiver criado um quiz e revisto três. |
|O utilizador autenticado seleciona o botão **"Logout"**.|-> O sistema não permite ao utilizador **terminar sessão**. |
