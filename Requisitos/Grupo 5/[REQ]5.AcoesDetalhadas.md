## Requisitos para a Arquitetura da Landing Page

# Autorias
* O utilizador autenticado acede ao identificador do quizz
* O utilizador autenticado consegue filtrar a informação que quer ver (quizzes aprovados, reprovados, pendentes ou nos rascunhos)
* O utilizador autenticado consegue saber se o quizz que criou foi aprovado, se não foi aprovado ou se está pendente de aprovação
* O utilizador autenticado consegues ver se tem quizzes por acabar , ou seja, se tem algo nos rascunhos

# Revistos
* O utilizador autenticado consegue ver quantos quizzes reviu  
* O utilizador autenticado consegues ver quantos quizzes estão por rever no geral 
* O utilizador autenticado consegue ver quantos quizzes foram criados no total
* O utilizador autenticado consegue ver quantos quizzes aprovou ou reprovou

# Resolvidos
* O utilizador autenticado consegue ver a quantos testes passou e a quantos chumbou
* O utilizador autenticado consegue ver a sua média de todos os testes

# Competências
* O utilizador autenticado consegue ver a média das suas respostas por categoria

# Ações
* O utilizador autenticado cliqua no botão criar quizzes e tem acesso à página de outro grupo
* O utilizador autenticado cliqua no botão criar testes e se não tiver revisto no mínimo 3 e criado 1 quiz não consegue ter acesso a essa página ( o botão vai estar bloqueado)
* O utilizador autenticado clique no botão rever quizzes e tem acesso à página de outro grupo

# Usuário
* O utilizador autenticado consegue terminar sessão 
