## Ver Status

# Primary Actor:
Utilizador autenticado

# Preconditions:
O utilizador está autenticado.
O utilizar autenticado está na landing page.

# Success Guarantee:
O sistema mostra ao utilizador as informações relativas à autoria de quizzes, à revisão de quizzes, à resolução de testes e ao seu perfil de competências.

# Main Success Scenario:
1. O utilizador visualiza quais os quizzes da sua autoria que estão aprovados, reprovados ou por rever.
2. O utilizador visualiza quantos quizzes aprovou, reprovou, reviu e pode rever.
3. O utilizador visualiza o número de testes onde obteve pontuação positiva e negativa e, ainda, a sua pontuação média e o número total de testes resolvidos.
4. O utilizador visualiza a percentagem que obteve em cada categoria.
5. O utilizador termina sessão.

# Extensions:

# Exception:
- 1a. O sistema não apresenta os dados relativos às autorias do utilizador.
- 1b. O utilizador ainda não criou nenhum quizz, logo o sistema não possui dados para mostrar.
- 2a. O sistema não apresenta os dados relativos aos quizzes aprovados e reprovados pelo utilizador, nem às estatísticas globais.
- 2b. O utilizador ainda não reviu nenhum quizz, logo o sistema não possui dados para mostrar.
- 3a. O sistema não apresenta os dados relativos aos testes que realizou.
- 3b. O sistema não calcula a média.
- 3c. O sistema não apresenta a média.
- 3d. O utilizador ainda não resolveu nenhum teste, logo o sistema não possui dados para mostrar.
- 4a. O sistema não apresenta o perfil de competências.
- 4b. O sistema não interpreta corretamente as diferentes tags referentes a cada tema.
- 4c. O sistema não apresenta o gráfico de aptidões corretamente.
- 4d. O utilizador ainda não resolveu nenhum teste logo o sistema não possui dados para mostrar.
