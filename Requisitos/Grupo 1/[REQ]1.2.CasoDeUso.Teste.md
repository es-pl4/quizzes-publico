[CASO DE USO #2]
* _Feature por implementar. Testes prontos a ser testados._

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O utilizador seleciona “Editar Quiz” na landing page.|- O sistema direciona o utilizador para a página de criação de quizzes, onde os campos de resposta já estão preenchidos com a informação do quiz reprovado.<br/>- O campo de pergunta está preenchido com “O que é o Git?”.<br/>- O primeiro campo de resposta está preenchido com “Sistema de controle de versões distribuído”.<br/>- O segundo campo de resposta está preenchido com ““Site de apostas online”.<br/>- O terceiro campo de resposta está preenchido com “Sistema de inteligência artificial”.<br/>- O quarto campo de resposta está preenchido com “Sistema de produção de criptomoeadas”.<br/>- O quinto campo de resposta está preenchido com “Aplicação de bem-estar”.<br/>- O sexto campo de resposta está preenchido com “Site de banco de dados”.|
|- O utilizador edita o campo da terceira resposta “Sistema de inteligência artificial” e substitui por “Sistema de criação de imagens através da inteligência artificial”.<br/>- O utilizador submete o quiz com a resposta editada.|- O sistema processa a alteração e avisa o utilizador que as alterações foram salvas.|


**-Possiveis- Extensions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O utilizador seleciona “Editar Quiz”|- O sistema direciona o utilizador para a página de criação de quizzes.<br/>- O campo de pergunta está preenchido com “O que é o Git?.<br/>- O primeiro campo de resposta está preenchido com ““Sistema de controle de versões distribuído”.<br/>- O segundo campo de resposta está preenchido com ““Site de apostas online”.<br/>- O terceiro campo de resposta está preenchido com “Sistema de inteligência artificial”.<br/>- O quarto campo de resposta está preenchido com “Sistema de produção de criptomoeadas”.<br/>- O quinto campo de resposta está preenchido com “Aplicação de bem-estar”.<br/>- O sexto campo de resposta está preenchido com “Site de banco de dados”.|
|- O utilizador edita o campo da terceira resposta “Sistema de inteligência artificial” e substitui por “Sistema de criação de imagens através da inteligência artificial”.<br/>- O utilizador submete o quiz com a resposta editada.|- O sistema não processa a alteração e avisa o utilizador que as alterações não foram salvas.|
|- O utilizador edita o campo da terceira resposta “Sistema de inteligência artificial” e substitui por campo em branco.<br/>- O utilizador submete o quiz com a resposta editada.|- O sistema rejeita a submissão ao detetar os campos de resposta por preencher e mantém o utilizador na mesma página.<br/>- O sistema assinala os campos a preencher a vermelho com a mensagem por baixo “Campo de preenchimento obrigatório”.|

