[CASO DE USO #1]

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O autor seleciona a opção “Criar quiz”.|- O sistema apresenta a página de criação de quiz. |
|- O autor preenche o campo de pergunta com “O que é o Git?”.<br/>- O autor preenche o primeiro campo com a resposta “Sistema de controle de versões distribuído”.<br/>- O autor preenche o segundo campo com a respostas “Site de apostas online”.<br/>- O autor preenche o terceiro campo com “Sistema de inteligência artificial”.<br/>- O autor preenche o quarto campo com a resposta “Sistema de produção de criptomoeadas”.<br/>- O autor preenche o quinto campo com a resposta “Aplicação de bem estar”.<br/>- O autor preenche o sexto campo com a resposta “Site de banco de dados”.<br/>- O autor seleciona como resposta correta a resposta do primeiro campo.<br/>- O autor preenche o campo de justificação para a resposta correta.<br/>- O autor carrega no botão de submissão para enviar o quiz.<br/>    | - O sistema aceita a submissão após confirmar o preenchimento correto dos campos.<br/>- O sistema guarda o quiz e manda para a fase de revisão.<br/>- O sistema cria uma notificação para o utilizador com “Quiz submetido!”. |


**-Possiveis- Extensions:**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O autor seleciona a opção “Criar quiz”.|- O sistema apresenta a página de criação de quiz.|
|- O autor preenche o campo de pergunta com “O que é o Git?”.<br/> - O autor carrega no botão de submissão para enviar o quiz.|- O sistema rejeita a submissão ao detetar os campos de resposta por preencher e mantém o utilizador na mesma página. <br/> - O sistema assinala os campos a preencher a vermelho com a mensagem por baixo “Campo de preenchimento obrigatório”.|
|- O autor preenche o primeiro campo com a resposta “Sistema de controle de versões distribuído”.<br/>- O autor preenche o segundo campo com a respostas “Site de apostas online”.<br/>- O autor preenche o terceiro campo com “Sistema de inteligência artificial”.<br/>- O autor carrega no botão de submissão para enviar o quiz.|- O sistema rejeita a submissão ao detetar campos por preencher e mantém o utilizador na mesma página.<br/>- O sistema assinala os campos a preencher a vermelho [por implementar]. com a mensagem por baixo “Campo de preenchimento obrigatório”.|
|- O autor preencher o quarto campo com a resposta “Sistema de produção de criptomoeadas”.<br/>- O autor preencher o quinto campo com a resposta “Aplicação de bem estar”.<br/>- O autor preencher o sexto campo com a resposta “Site de banco de dados”.<br/>- O autor carrega no botão de submissão para enviar o quiz.|- O sistema rejeita a submissão ao detetar campos por preencher e mantém o utilizador na mesma página.<br/>- O sistema assinala os campos a preencher a vermelho [por implementar]. com a mensagem por baixo “Campo de preenchimento obrigatório”.|
|- O autor seleciona como resposta correta a resposta do primeiro campo.<br/>- O autor preenche o campo de justificação para a resposta correta.<br/>- O autor carrega no botão de submissão para enviar o quiz.|- O sistema aceita a submissão após confirmar o preenchimento correto dos campos.<br/>- O sistema manda a pergunta para a fase de revisão.<br/>- O sistema cria uma notificação para o utilizador com “Quiz submetido!”|
