## Caso de Uso #2
“Editar Quiz”

**Primary Actor:** 
Autor

**Level:**
Sea

**Stakeholders and Interests:** 
Autor

**Preconditions:** 
O utilizador está autenticado. 
O utilizador acedeu à página de criação de quizzes. O utilizador já tem quizzes da sua autoria na aplicação.

**Success Guarantee:** 
O sistema recebe a alteração de um dos textos preenchidos nos campos de resposta submetidos corretamente, adiciona-a ao conjunto de perguntas por rever com a nova versão, e informa o utilizador que as alterações foram guardadas.

**Main Success Scenario:** 
0. O autor seleciona a opção "Editar Quiz".
1.	O sistema apresenta a página de criação de quiz, com os campos preenchidos com as respostas dadas previamente pelo autor.
2.	O autor apaga o(s) campo(s) que pretende alterar e preenche-o(s) com a nova informação.
3.	O autor submete novamente o quiz ao sistema.
4.	O sistema verifica que todos os campos estão preenchidos corretamente.
5.	O sistema altera o quiz com as novas mudanças que o autor fez.
6.	O sistema mantém o quiz ao conjunto de perguntas por rever.
7.  O sistema redireciona o criador de quizzes para a landing page.

**Extensions:**
- 3a. O autor cancela a submissão do formulário e retorna à landing page.
- 9a. O autor abandona a página de criação de quiz.

**Exceptions:**
- 2a. O autor não preenche o(s) campo(s) que pretendeu alterar.
- 4a. O sistema não verifica um ou mais campos.
- 4b. O sistema não deteta campos não preenchidos.
- 5a. O sistema não guarda o quiz com as alterações. 
- 6a. O sistema não adiciona o quiz alterado ao conjunto de perguntas por rever.
