[CASO DE USO #1]

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O autor preenche o campo de pergunta com “O que é o Git?”.<br/>- O autor preenche o primeiro campo com a resposta “Sistema de controle de versões distribuído”.<br/>- O autor preenche o segundo campo com a respostas “Site de apostas online”.<br/>- O autor preenche o terceiro campo com “Sistema de inteligência artificial”.<br/>- O autor preenche o quarto campo com a resposta “Sistema de produção de criptomoeadas”.<br/>- O autor preenche o quinto campo com a resposta “Aplicação de bem estar”.<br/>- O autor preenche o sexto campo com a resposta “Site de banco de dados”.<br/>- O autor seleciona como resposta correta a resposta do primeiro campo.<br/>- O autor preenche o campo de justificação para a resposta correta.<br/>- O autor seleciona a tag “Arquitetura e Design”.<br/>- O autor carrega no botão de submissão para enviar o quiz.|- O sistema aceita a submissão após confirmar o preenchimento correto dos campos.<br/>- O sistema guarda o quiz e manda para a fase de revisão.|
