[CASO DE USO #3]

**Main success scenario**

|  Ação      | Resultado   |
| :---      |        :--- |
|- O utilizador seleciona “Editar Quiz” na landing page.|- O sistema direciona o utilizador para a página de criação de quizzes, onde os campos de resposta já estão preenchidos com a informação do quiz reprovado.<br/>- O campo de pergunta está preenchido com “O que é o Git?”.<br/>- O primeiro campo de resposta está preenchido com “Sistema de controle de versões distribuído”.<br/>- O segundo campo de resposta está preenchido com ““Site de apostas online”.<br/>- O terceiro campo de resposta está preenchido com “Sistema de inteligência artificial”.<br/>- O quarto campo de resposta está preenchido com “Sistema de produção de criptomoeadas”.<br/>- O quinto campo de resposta está preenchido com “Aplicação de bem-estar”.<br/>- O sexto campo de resposta está preenchido com “Site de banco de dados”.|
|- O utilizador carrega no botão “apagar” para apagar todos os campos de resposta preenchidos.|- O sistema processa o pedido e apaga os campos de resposta, deixando-os vazios.|
