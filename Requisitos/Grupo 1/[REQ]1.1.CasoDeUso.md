## Caso de Uso #1
"Criar Quiz"

**Primary Actor:**
Autor

**Level:**
Sea

**Stakeholders and Interests:**
Autor

**Preconditions:**
O utilizador está autenticado.
O utilizador acedeu à página de criação de quizzes.

**Success Guarantee:**
O sistema recebe uma pergunta submetida corretamente, adiciona-a ao conjunto de perguntas por rever, e informa o utilizador que a submissão foi aceite.

**Main Success Scenario:**
0. O autor seleciona a opção "Criar Quiz".
1. O sistema apresenta a página de criação de quiz.
2. O autor preenche corretamente todos os campos necessários à submissão do quiz.
3. O autor submete o quiz ao sistema.
4. O sistema verifica que todos os campos estão preenchidos corretamente. 
5. O sistema guarda o quiz.
6. O sistema adiciona o quiz ao conjunto de perguntas por rever.
7. O sistema redireciona o criador de quizzes para a landing page.

**Extensions:**
- 3a. O autor cancela a submissão do formulário e retorna à landing page.
- 7a. O autor abandona a página de criação de quiz.

**Exceptions:**
- 2a. O autor não preenche todos os campos do formulário.
- 2b. O autor não preenche corretamente um ou mais campos.
- 4a. O sistema não verifica um ou mais campos.
- 4b. O sistema não deteta campos não preenchidos.
- 4c. O sistema verifica incorretamente um ou mais campos.
- 5a. O sistema não guarda o quiz.
- 6a. O sistema não adiciona o quiz ao conjunto de perguntas por rever.

