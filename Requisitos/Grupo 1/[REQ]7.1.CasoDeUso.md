## Caso de Uso #1
“Selecionar Tag”

**Referente à User Story 7:** 
Como criador de quizzes quero poder seleccionar uma ou mais tags --de um conjunto pré-definido-- aquando da criação de um quizz para que os testes possam ser criados com base nessas tags.

**Primary Actor:** 
Criador de Quizzes
**Level:**
Sea
**Stakeholders and Interests:**
Criador de Quizzes
**Preconditions:** 
O utilizador está autenticado. 
O utilizador acedeu à página de criação de quizzes. O utilizador está na página de “Criar Quiz” com os campos preenchidos.

**Success Guarantee:** 
O sistema recebe uma pergunta submetida corretamente, à qual foi associada uma tag selecionada pelo criador de quizzes. Esta é adicionada ao conjunto de perguntas por rever.

**Main Success Scenario:** 
0. O criador de quizzes seleciona a opção "Criar Quiz".
1.	O sistema apresenta a página de criação de quiz.
2.	O criador de quizzes preenche corretamente os campos necessários à submissão do quiz.
3.	O criador de quizzes seleciona a tag desejada.
4.	O criador de quizzes submete o quiz ao sistema.
5.	O sistema verifica que todos os campos estão preenchidos corretamente e que uma tag foi selecionada.
6.	O sistema guarda o quiz.
7.	O sistema adiciona o quiz ao conjunto de perguntas por rever.
8.	O sistema redireciona o criador de quizzes para a landing page.


**Extensions:**
* --

**Exceptions:**
* 3a. O criador de quizzes não seleciona nenhuma tag.
* 4a. O sistema não verifica se uma tag foi selecionada.
* 6a. O sistema não guarda a tag selecionada.
