## Caso de Uso #3
“Apagar Quiz”

**Primary Actor:** Autor <br/>

**Level:** Sea <br/>

**Stakeholders and Interests:** Autor <br/>

**Preconditions:** O utilizador está autenticado. O utilizador acedeu à página de criação de quizzes. O utilizador está na página de “Criar Quiz” com os campos preenchidos. <br/>

**Success Guarantee:** 
Após a instrução do autor, o sistema limpa todos os campos de resposta, permitindo o autor que comece o processo de criar quiz de novo.

**Main Success Scenario:** 
1.	O autor seleciona a opção "Apagar Quiz" na página de “Criar Quiz” com os campos preenchidos com as respostas dadas previamente pelo autor.
2.	O sistema recarrega a página de “Criar Quiz” com os campos de resposta vazios.

**Extensions:**
* --

**Exceptions:**
* 2a. O sistema não apaga os campos de resposta após recarregar a página.
