**[CASO DE USO 3.2 - Visualizar o Top 10 no Hall of Fame]**

**Primary Actor:**

Utilizador Autenticado

**Level:**

Kite

**Stakeholders and Interests:**

Autor e revisor

**Preconditions:**

1. O utilizador está autenticado
2. O utilizador está na Landing Page. 

**Success Guarantee:**

1. O utilizador acessa a Landing Page e visualiza o Hall of Fame.
2. É exibido o ranking dos utilizadores com maior número de Quizzes certos. 


**Main success scenario:**

Ação

 O utilizador autenticado visualiza a secção do site correspondente ao Hall of Fame. 

Resultado

 O sistema apresenta a secção do site correspondente ao Hall of Fame, na Landing Page, com o Ranking de utilizadores da plataforma. 

- Número do Rank;
- Username do utilizador ranqueado; 


Esses dados estão respectivamente dispostos em fila horizontal, cada fileira corresponde aos dados do utilizador ranqueado.

As fileiras com os dados do utilizador são mostradas em ordem crescente, verticalmente. O sistema possibilita visualizar todos os utilizadores colocados, da primeira à décima colocação.

**Extension:**

-  Em caso de empate, as fileiras com a mesma colocação são mostradas pelo sistema de maneira seguida, por ordem alfabética. 
- Se não houver usuários suficientes para as 10 colocações do Hall of Fame, o sistema mostra apenas aquelas que já existem.

