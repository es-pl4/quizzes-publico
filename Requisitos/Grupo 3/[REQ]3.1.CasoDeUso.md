## Caso de Uso #1
### Caso de Uso: Visualizar se a resposta dada está correta e a respetiva justificação

#### Primary Actor: 
Utilizador Autenticado

#### Level:
Kite 

#### Stakeholders and Interests: 
Autor e revisor 

#### Preconditions: 
- O utilizador está autenticado. 
- O utilizador resolveu o teste. 
- O utilizador acedeu à página de soluções do teste. 

#### Success Guarantee: 
- O utilizador obtém a indicação que a sua resposta está correta ou incorreta. 
- É exibido, se a resposta escolhida está correta ou errada e a sua respetiva justificação. Se o utilizador não tiver respondido. não aparece nada. 

#### Main Sucess Scenario:
1. O sistema abre uma página à parte para mostrar ao utilizador as suas respostas.
2. O sistema sinaliza as respostas dadas a cada quizz como certas ou erradas.
3. É dado um ponto caso o utilizador tenha respondido corretamente. Não lhe é atribuído nenhum ponto caso tenha errado ou não tenha respondido. 
5. O sistema emite a pontuação final.
6. Existe um botão no fim da página para o Wall of fame. 
7. Caso o utilizador carregue no botão, o mesmo é levado para o Wall of Fame.

#### Extensions: 

2a. Caso o utilizador autenticado responda certo, é mostrado que a sua resposta está correta.<br>
2b. Caso o utilizador autenticado responda errado, é mostrado que a sua resposta está errada.<br>
2c. Caso o utilizador autenticado não responda, não é mostrado nada.<br>
3a. Um utilizador que não responde à pergunta não recebe pontos.<br>
3b. Um utilizador que responde errado não recebe pontos.<br>
5a. O sistema indica se o utilizador aprova ou reprova o teste conforme a pontuação obtida (>=50% aprova; <50% reprova)
