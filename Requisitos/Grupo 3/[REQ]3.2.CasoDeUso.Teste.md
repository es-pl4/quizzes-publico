## TESTE DE QUALIDADE

**USER STORY/FEATURE:**<br>
Como utilizador autenticado quero ver a solução às respostas do quizz que realizei de forma a que forneça os conhecimentos pretendidos.

**CASO DE USO:**<br>
Visualizar o Top 10 no Hall of Fame


**ESPECIFICAÇÃO DO TESTE:**

**TESTER:**
[Rita Simões](https://gitlab.com/ritasimoes)

**Nº DE TESTE:**
1


| AÇÃO | RESULTADO |
|--|--|
| Colocar as credenciais do Utilizador Autenticado na página de Login. | A página carrega sem dar erro, se as instruções estiverem corretas.||--|--|
| Verificar a sua pontuação média no Hall of Fame. |A página mostra a pontuação média do Utilizador Autenticado, apenas se este estiver no Top 10.||--|--|
| Verificar a posição do Utilizador Autenticado no Hall of Fame. | A página mostra o nome do utilizador, se este estiver no Top 10. |A página mostra a posição do Utilizador Autenticado, se este estiver no Top 10.|--|--|
| Verificar a posição dos outros Utilizadores Autenticados em relação à posição do Utilizador. | A página mostra os Utilizadores Autenticados acima e abaixo do Utilizador Autenticado||--|--|

**APROVADO/NÃO APROVADO:**<br>
Aprovado

**JUSTIFICAÇÃO:**<br>
Todas os testes foram aprovados com sucesso.

**OBSERVAÇÕES:**<br>
N/A
