## TESTE DE QUALIDADE

**USER STORY/FEATURE:**<br>
Como utilizador autenticado quero ver a solução às respostas do quizz que realizei de forma a que forneça os conhecimentos pretendidos.

**CASO DE USO:**<br>
Visualizar se a resposta dada está correta e a respetiva justificação.

**ESPECIFICAÇÃO DO TESTE:**

**TESTER:**
[Mariana Pombo](https://gitlab.com/es-pl4/projeto-es/-/blob/main/%5BPM%5D%20Gestão%20Projeto/Profiles/Grupo-3/Mariana_Pombo.md) e [Maria Lima](https://gitlab.com/es-pl4/projeto-es/-/blob/main/%5BPM%5D%20Gestão%20Projeto/Profiles/Grupo-3/Maria%20Lima.md)

**Nº DE TESTE:**
1

| Ação | Resultado | Comentário |
| ------ | ------ | ------ |
|    O Utilizador Autenticado abre a página    |    O sistema carrega página carrega sem dar erro.    |  Aprovado <br> A página carrega sem bugs.   |
|    O Utilizador Autenticado quer verificar se a sua resposta à pergunta está incorreta    |    É apresentada no ecrã a mensagem - "insuficiente" - indicando que a resposta está incorreta, com a cor vermelha, logo abaixo há um gráfico com a pontuação atual do Utilizador |   Aprovado<br> Comentário: Em todas as respostas erradas o fundo é vermelho e há uma mensagem que diz - "na próxima vai correr melhor"|
|    O Utilizador Autenticado quer verificar se a sua resposta à pergunta está correta   |    É apresentada no ecrã a mensagem - "aprovado" - indicando que a resposta está correta, com a cor verde, logo abaixo há um gráfico com a pontuação atual do Utilizador    |   Aprovado<br> Comentário: Em todas as respostas erradas o fundo é verde e há uma mensagem que diz - "Muito bem"  | 
  O Utilizador Autenticado deseja ver todas as respostas possiveis do teste, a indicar se está correto ou incorreto e com sua respectiva justificação. |    O sistema mostra as respostas de maneira seguida, es respostas erradas possuem a cor vermelha e as corretas a cor verde, logo abaixo há as respectivaa justificações |  Aprovado <br> |
|   O Utilizador Autenticado pretende verificar se aparece o resultado do questionário e a sua respetiva pontuação    |   -> No sistema de pontuação, se contabilizam todas as respostas certas.<br> -> Por cada resposta errada ou não respondida não contabiliza pontos.<br> -> Se metade ou mais de metade das perguntas estiverem corretas o resultado é aprovado <br> -> se responder mais de metade das perguntas erradamente está reprovado.<br> -> se responder metade certo, metade errado, o resultado será aprovado. |   Aprovado<br> - Confirma-se que no sistema de pontuação, se contabilizam todas as respostas certas.<br>- Confirma-se que por cada resposta errada ou não respondida não contabiliza pontos.<br>- Confirma-se que se metade das perguntas estiverem corretas o resultado é aprovado;<br>- Confirma-se que se responder mais de metade das perguntas erradamente está reprovado.      |
|    O Utilizador Autenticado pretende que as perguntas surjam identificadas(numeradas)   |    As perguntas encontram-se corretamente identificadas, estando numeradas consoante a ordem a que são respondidas no questionário.   |  Aprovado<br> Comentários: - As perguntas encontram-se corretamente identificadas, estando numeradas consoante a ordem a que são respondidas no questionário.   |
|    O Utilizador Autenticado quer que o número de soluções obtidas corresponda ao número de perguntas do teste.  |    O número de respostas que foi submetido corresponde ao número de perguntas, não tendo sido submetida nenhuma resposta incompleta, isto é, por responder.   |  Aprovado<br> Comentários: - O número de respostas que foi submetido corresponde ao número de perguntas, não tendo sido submetida nenhuma resposta incompleta, isto é, por responder.    |
|    O Utilizador Autenticado pretende que a ordem de obtenção dos resultados corresponda à ordem de realização das perguntas.   |    A ordem em que os resultados aparecem na página corresponde à ordem em que a pessoa que está a realizar o quizz e submete as suas respostas.   |   Aprovado<br> Comentários: - A ordem em que os resultados aparecem na página corresponde à ordem em que a pessoa que está a realizar o quizz e submete as suas respostas.   |


**Extensions**

| Ação | Resultado | Comentário |
| ------ | ------ |------ |
|    O Utilizador Autenticado submete um teste com um ou mais quizzes por responder   |    Apesar de surgir a pergunta e as suas opções de resposta, o sistema não apresenta qualquer correção ou justificação deste quizz.    |    Aprovado<br> Comentário: Não surge correção das respostas nem justificação. 

**APROVADO/NÃO APROVADO:**<br>
Aprovado

**JUSTIFICAÇÃO:**<br>
Todas os testes foram aprovados com sucesso.

**OBSERVAÇÕES:**<br>
N/A


