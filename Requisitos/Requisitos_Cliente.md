## LISTA DE REQUISITOS DO CLIENTE
Levantamento de requisitos do cliente na aula teórica de Engenharia de Software (por fala, não registado):
- Uma plataforma de testes com quizzes sobre a disciplina; 
- Quiz: uma pergunta com 6 respostas; 
- Todas as respostas devem ter uma justificação de porque é que estão certas/ erradas; 
- Cada teste deve ter 4 quizzes (numa primeira versão, 1 quiz = 1 teste);
 - Login;
 - Quem avalia não pode alterar o quiz; 
- Quizzes não são aleatórios por teste;
 - O user deve avaliar quizzes, de modo a poder aceder à funcionalidade de criar quizzes;
 - Um quiz só pode ser aprovado e colocado na plataforma após a aprovação de 3 users; 
- O quiz tem que estar completo; 
- Há uma página para ver o desempenho do utilizador/estado do website
- (...)

TRADUÇÃO PARA USER STORIES: <br>
**USER STORY 1 (atribuída ao GRUPO 1)** <br>
“Enquanto autor, quero registar um quiz para ser revisto.” <br>
**USER STORY 2 (atribuída ao GRUPO 2)** <br>
“Enquanto revisor, quero rever um quizz para dar seguimento a esse mesmo quiz.” <br>
**USER STORY 3 (atribuída ao GRUPO 3)** <br>
“Como utilizador autenticado quero ver a solução às respostas do quizz que rea lizei de forma a que forneça os conhecimentos pretendidos.” <br>
**USER STORY 4 (atribuída ao GRUPO 4)** <br>
“Enquanto utilizador autenticado, pretendo resolver as questões do quiz de for ma a testar os meus conhecimentos.” <br>
**USER STORY 5 (atribuída ao GRUPO 5)** <br>
“Enquanto utilizador autenticado quero uma landing page para aceder aos meus status e ações.” <br>
**USER STORY 6 (atribuída ao GRUPO 5)** <br>
“Enquanto utilizador quero fazer login/registar para aceder à Landing Page.” <br>


[DIA 04/11/2022 - ATUALIZAÇÃO DE REQUISITOS]

Requisitos recebidos por mensagem no canal #general da plataforma SLACK, pelo docente/cliente: <br>
- Os quizzes deviam encaixar-se em 12 TAGs definidas pelo docente;
- Um teste consiste num conjunto de quizzes (N = 20).
- Cada quizz só pode estar incluído, no máximo, em dois testes.
- A criação de testes implica selecionar as tags dos temas que se deseja incluir no teste. Por exemplo, selecionando apenas as tags “PM e REQ” a aplicação vai à lista de quizzes disponíveis e escolhe aleatoriamente 10 (N/2) quizzes que tenham a categoria PM e 10 (N/2) quizzes que tenham a categoria REQ (usem aritmética de inteiros).
- Se não houver quizzes disponíveis de uma dada categoria, o número de quizzes de um tipo ‘em falta’ passa para as restantes categorias, por qualquer ordem.
- Se não houver nenhum quizz em nenhuma das categorias seleccionadas, o teste não é criado e o criador  é notificado disso.
- Quando um teste acaba de ser criado, o criador de testes tem de poder saber qual a distribuição efectiva de quizzes por tags (e.g. 10xREQ, 5xPM, 3xIMP, 2xTST). vii. Aliás, esse ‘perfil’ de tags tem de fazer parte da descrição do teste para os ‘resolvedores’ poderem escolher os testes que querem realizar.
- Um teste tem de ter um ‘título’ (e.g. “Teste Geral”, “Anda testar se és um dev”, “Teste para frontenders”, “Teste do Pixinguinha”,...)
- O criador de testes não vê as questões que foram incluídas, apenas vê o perfil de tags (e o título que ele próprio deu).
- O utilizador pode selecionar um teste e resolvê-lo e ver a sua autoavaliação no Hall of Fame
- O utilizador pode ver a sua classificação e desempenho no seu perfil
- O administrador pode exportar testes da plataforma que está a usar;
- O administrador pode importar testes de outras plataformas para a que está a usar.»

TRADUÇÃO PARA USER STORIES: <br>
**USER STORY 7 (atribuída ao GRUPO 1)** <br>
Como criador de quizzes quero poder seleccionar uma ou mais tags --de um 
conjunto pré-definido-- aquando da criação de um quizz para que os testes pos sam ser criados com base nessas tags <br>
**USER STORY 8 (atribuída ao GRUPO 4)** <br>
Como criador de testes quero poder criar um teste com base em tags para 
que esta comunidade possa fazer auto-avaliação e eu me sentir um boss por ter 
sido eu a criar um teste que todos os meus colegas vão fazer <br>
**USER STORY 9 (atribuída ao GRUPO 5)** <br>
Como resolvedor quero poder seleccionar um teste e resolvê-lo para fazer 
a minha auto-avaliação e, quem sabe, ir para o top10 do ‘hall of fame’ <br>
**USER STORY 10 (atribuída ao GRUPO 5)** <br>
Como resolvedor quero ter no meu perfil o número de quizzes de uma dada 
categoria (tags) que acertei para saber quais os meus pontos fortes sim, se um 
quizz tiver mais que uma tag, ao acertar nesse quizz acrescento pontos em mais que 
uma categoria <br>
**USER STORY 11 (atribuída ao GRUPO 2)** <br>
Como admin quero poder exportar quizzes em XML para ser possivel ex portar quizzes feitos na minha PL --claramente os melhores!-- para a aplicação de outras turmas <br>
**USER STORY 12 (atribuída ao GRUPO 2)** <br>
Como admin quero poder importar quizzes em XML para ser possivel im portar quizzes criados noutra PL para a minha aplicação --que é muito mais fixe que as das outras turmas-- <br>

