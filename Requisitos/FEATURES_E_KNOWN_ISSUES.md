# FEATURES IMPLEMENTADAS
Estas são as features que estão implementadas e prontas para testar nesta versão do “QuizzES”.
### User Story 1:
* Criar um quiz e submeter:
    * Impede de proceder se não tiver resposta correta selecionada;
    * Impede de proceder se não tiver campos preenchidos;
* Editar um quiz e submeter com informação nova:
     * A justificação de reprovação aparece;
    * Impede de proceder se não tiver resposta correta selecionada; 
     * Impede de proceder se não tiver campos preenchidos;
     * A informação é alterada na base de dados;
    * O teste “Reprovado” volta a ficar “Por aprovar”;
* Apagar os campos de resposta do quiz:
    * Os campos ficam vazios na página de editar/criar; 
    * As respostas voltam à informação anterior;

### User Story 2:
* Selecionar um quiz para rever;
* Aprovar um quiz:
    * Confirmar a aprovação ao quiz;
    * O quiz deixa de aparecer na página de “Rever Quiz”;
* Reprovar um quiz:
    * Confirmar a reprovação do quiz;
    * Justificar a reprovação no campo de resposta;
    * O quiz deixa e aparecer na página de “Rever Quiz”

### User Story 3: 
* Ver soluções do quiz realizado:
    * Mostra o resultado do teste: aprovado ou não aprovado;
    * Aparece (em baixo) os mesmos quizzes respondidos, desta vez com as respostas avaliadas em certas/erradas e as respetivas justificações 
### User Story 4:
* Resolver um teste:
    * Aparece testes com 5, 10 e 15 quizzes com as respetivas perguntas e respostas num só teste; 
    * Permite responder errado e será contabilizado;

### User Story 5:
* Landing page:
    * Botão de Criar Quiz;
    * Botão de Rever Quiz;
    * Botão de Reprovado -> O botão de “Reprovado” na criação direciona para a página “Editar Quiz”
    * Botão Criar teste;
    * Botão Importar/Exportar;
    * Logout;
* Status dos testes: por aprovar, reprovado, aprovado (leva à página de Editar quiz no “Reprovado”);
* Nº de testes aprovados e reprovados pelo utilizador e gerais por rever;
    * O botão de “Resolver Teste” desbloqueia após o número pedido de revisões do novo utilizador (3 revisões); 
* Pontuação dos testes aprovados e reprovados e número total de testes feitos;

### User Story 6:
* Registar:
    * Regista os dados do novo utilizador;
    * Impede que o utilizador não preencha campos;
    * Informa se já existe dados iguais aos que o utilizador tenta adicionar;
    * Informa que o utilizador foi registado com sucesso;
* Login:
    * Impede que o utilizador não preencha campos;
    * Informa se os dados estão errados;
    * Direciona para a Landing Page;

### User Story 7: 
* Categorizar um quiz com as 12 tags disponíveis:
    * A tag fica selecionada e visível;
    * A tag é alterável em “Editar Quiz”;
### User Story 9:
* Ver o Hall of Fame:  
    * A pontuação dos utilizadores está funcional;
    * Os primeiros três lugares têm uma distinção visual com os restantes;
    * Os utilizadores com a mesma pontuação aparecem empatados no mesmo lugar e ordenados por ordem alfabética;
    * O utilizador que aparece a seguir aos utilizadores empatados incrementa um valor no lugar onde se encontra no quadro;

### User Story 10 (incorporada na User Story 5):
* Pontuação de testes resolvidos por categoria visível na Landing Page (relativo às tags);

### User Story 11:
* Exportar testes do “QuizzES” para outras plataformas;



# KNOWN ISSUES:
Estas são features não implementadas/implementadas de forma incompleta nesta versão e que estão a ser trabalhadas. Por esse motive, **não estão preparadas para testagem neste momento**.

### User Story 8:
* Criar teste:
    * Nº de quizzes e categorias a ser incluídas;
    * O utilizador escolhe um teste entre 5/10/15 quizzes;
    * Gerar teste com base nas categorias;


### User Story 12:
* Importar quizzes de outras plataformas para o QuizzES (neste momento só consegue ser guardado numa pasta) 
* São colocados na BD e ficam disponíveis para resolver;
