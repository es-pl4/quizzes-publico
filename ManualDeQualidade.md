# Manual de Qualidade - "QuizzES"
**Grupo de Qualidade Engenharia de Software <br>
PL4 <br>
Ano Letivo 2022/2023**

## Indíce

**1.Introdução** 
* 1.1. O manual para o projeto  
* 1.2. Considerações 

**2. PL4** 
* 2.1. Informação Geral 
* 2.1.1. Subgrupos 
* 2.2. Grupos Transvessais 

**3. O Projeto** 
* 3.1. Enunciado 
> 3.1.1.Requisitos 
> 3.1.2.Requisitos para User Stories 
* 3.2. Gestão de Projeto 
> 3.2.1.Repositório GitLab  
> 3.2.2.Requisitos 
> * 3.2.2.1.User Stories 
> * 3.2.2.2.Casos de Uso e Testes 
> 3.2.3.Arch 
* 3.2.4.Development 
* 3.2.5.Deployment 
* 3.2.6.Outros 
* 3.2.7.Templates 
> * 3.2.7.1.Atas 
> * 3.2.7.2.Issues Gerais 
> * 3.2.7.3.Commits 
> * 3.2.7.4.Scripts 
> * 3.2.7.5.User Story 
> * 3.2.7.6.Caso De Uso 
* 3.2.8.Nomenclaturas 
> * 3.2.8.1.[DEV] Código 
> * 3.2.8.2.[PM] Gestão Projeto 
> * 3.2.8.3.[REQ] Requisitos 
> * 3.2.8.4.[QA] Testes 
* 3.3. QuizzES
> 3.3.1. O Produto 
> 3.3.2. Qualidade do Produto 

**4. Anexos** 
* 4.1. Contactos da PL 
* 4.2. Plataformas 
 
<br>

## 1.Introdução

### 1.1.O manual para o projeto
Este Manual de Qualidade foi criado no âmbito da cadeira de Engenharia do Software 2022, mais especificamente para o projeto nomeado “QuizzES” da turma prática PL4. Neste documento está contida informação sobre a PL em si, o seu funcionamento, e os seus processos ao longo do semestre para o desenvolvimento do projeto de uma plataforma para resolver, criar e rever questões sobre a disciplina ES. <br>
Esperemos que, da melhor maneira possível, este guia possa ser útil a quem leia e a quem queira aprofundar-se mais sobre o nosso projeto.

### 1.2. Considerações
Esta é a versão 1.2 do Manual de Qualidade, disponível em PDF e Ficheiro Markdown no repositório da disciplina (QuizzES). A sua elaboração foi feita por alguns membros do grupo de Qualidade da PL, nomeadamente Daniela Mendes, Francisco Ventura, Margarida Tonel e Gonçalo Pires, e aprovada pela turma PL4.

* “Manual de Qualidade - QuizzES” v1.1 - 03/12/2022

* “Manual de Qualidade - QuizzES” v1.2 - 03/12/2022

* “Manual de Qualidade - QuizzES” v1.3 - 05/12/2022

* "Manual de Qualidade - Quizzes v1.4. - 12/12/2022

## 2. PL4

### 2.1. Informação Geral
A PL4 é uma turma prática pertencente à disciplina de Engenharia de Software no ano letivo de 2022, com Mário Rela como regente da cadeira. É lecionada à segunda-feira, das 16H às 18H, pelo professor António Damasceno, e é composta por 32 alunos: 29 membros da licenciatura de Design e Multimédia e 3 membros da licenciatura de Engenharia Informática.

**Lista de alunos - PL4:**
- Ana Pimentel
- Andreia Silva
- Catarina Anselmo
- Constança Correia
- Daniela China 
- Daniela Mendes
- Débora Patrício   
- Diogo Silva   
- Ema Gomes   
- Ester Gaio  
- Filipa Ferreira 
- Filipe Viana   
- Filipa Vilhena 
- Francisco Ventura   
- Gonçalo Figueiredo   
- Gonçalo Pires
- Guilherme Almeida
- Guilherme Costa
- Guilherme Lopes
- Inês Torres
- João Mendes
- João Simões
- José Porto
- Mafalda Almeida
- Marco Braga
- Margarida Figueiredo
- Margarida Mendonça
- Maria Lima
- Mariana Pombo
- Pedro Monteiro
- Rita Simões
- Rui Moniz
- Tomás Oliveira
- Tomás Ferreira

### 2.1.1. Subgrupos
> _«Quero reforçar uma ideia (...) sobre a organização dentro de cada PL: cada subgrup é responsável pela implementação “end-to-end” das user stories que adoptar. Ou seja, não há um grupo de back-end, outro de front-end, outro de testes, outro de integração, outro de deploy. NÃO! Cada grupo tem de fazer a implementação completa da sua user-story, ou seja tem de tratar do back-end, front-end e etc. Nós, docentes, vamos ver se a user-story de cada (sub)grupo está implementada, independentemente dos outros subgrupos.»_

- Professor Mário Rela, no canal #general do grupo SLACK - ES2022, dia 6/10/2022

Deste modo, no início do semestre, a PL4 decidiu organizar-se em grupos com base nos 5 requisitos iniciais estabelecidos, para que cada um cumprisse as features a serem trabalhadas de uma forma equilibrada.

**Grupo 1 (7 membros)**
- Filipa Vilhena
- Guilherme Almeida
- Daniela Mendes
- Diogo Silva
- Filipa Ferreira
- Francisco Ventura
- Marco Braga

**Grupo 2 (7 membros)**
- Constança Correia
- Ana Pimentel
- Andreia Silva
- Daniela China
- Débora Patrício
- Ester Gaio
- José Porto

**Grupo 3 (6 membros)**
- Maria Lima
- Margarida Mendonça
- Ema Gomes
- Mafalda Almeida
- Mariana Pombo
- Rita Simões

**Grupo 4 (7 membros)**
- Tomás Oliveira
- Rui Moniz
- Filipe Viana
- Gonçalo Figueiredo
- Gonçalo Pires
- João Mendes
- Tomás Ferreira

**Grupo 5 (7 membros)**
- Catarina Anselmo
- Guilherme Costa
- Guilherme Lopes
- Inês Torres
- João Simões
- Margarida Figueiredo
- Pedro Monteiro

### 2.2.2.Grupos tranversais
No dia 19/09/2022, a PL decidiu criar grupos transversais: grupos que abordariam as diversas áreas/etapas do projeto, compostos por 1 membro de cada um dos diversos grupos já estabelecidos previamente.

> **DEPLOY:** <br>
Os membros de Deploy estão encarregues da fase mais avançada de combinar elementos como código para uma só máquina/branch. Coordenam sobretudo os aspetos técnicos do projeto e auxiliam os colegas do próprio subgrupo em dúvidas e problemas de código/ficheiros. <br>

Membros:
- Guilherme Almeida (Grupo 1)
- Ana Pimentel (Grupo 2)
- Margarida Mendonça (Grupo 3)
- Rui Moniz (Grupo 4) 
- Guilherme Costa (Grupo 5)

> **IDENTIDADE:** <br>
Em Identidade, os membros estão encarregues da parte visual do produto. São estes que trabalham para desenhar uma primeira estrutura base dos elementos de cada página da aplicação (apresentados em mockups) e desenvolvem uma identidade mais composta para a marca “QuizzES”. <br>

Membros:

- Diogo Silva (Grupo 1)
- Débora Patrício (Grupo 2)
- Mafalda Almeida (Grupo 3)
- Gonçalo Pires (Grupo 4)
- Guilherme Lopes (Grupo 5)

> **GESTOR DE GITLAB/DISCORD:** <br>
Os gestores são os membros encarregues de manter a organização do repositório GITLAB e no server de Discord, e ajudar a coordenar os restantes colegas no que toca na gestão de ficheiros. <br>
Membros:
- Guilherme Almeida (grupo 1)
- Guilherme Costa (Grupo 5)

> **IMPLEMENTAÇÃO:** <br>
O trabalho da implementação passa por fazer a ponte de ligação entre o trabalho do Deploy e da Identidade: aplicar o que ficou decidido a nível visual para o código que já esteja combinado e funcional. <br>

Membros:
- Filipa Ferreira (Grupo 1)
- Ester Gaio (Grupo 2)
- Ema Gomes (Grupo 3)
- Gonçalo Figueiredo (Grupo 4)
- Pedro Monteiro (Grupo 5)

> **SCRUM MASTERS:** <br>
Os Scrum Masters são um membro selecionado de cada subgrupo que serve como porta-voz/meio de comunicação entre os agrupamentos da PL. Têm funcionado como um manager que, em reuniões entre Scrum Masters, apresentam o que o seu grupo tem trabalhado, as suas dificuldades e tentam solucionar, em conjunto, aspetos gerais para o funcionamento de todos os grupos. <br>

Membros:
- Filipa Vilhena (Grupo 1)
- Constança Correia (Grupo 2)
- Maria Silva (Grupo 3)
- Tomás Oliveira (Grupo 4)
- Catarina Anselmo (Grupo 5)

> **Dia 04/11/2022 - ATUALIZAÇÃO DE MEMBROS NOS GRUPOS DE  IDENTIDADE/IMPLEMENTAÇÃO**

**IDENTIDADE:** <br>
Membros:

- Diogo Silva (Grupo 1)
- Débora Patrício (Grupo 2)
- Mafalda Almeida (Grupo 3)
- **Tomás Ferreira (Grupo 4)**
- **Pedro Monteiro (Grupo 5)**
  
**IMPLEMENTAÇÃO:** <br>
Membros:
- Filipa Ferreira (Grupo 1)
- Ester Gaio (Grupo 2)
- Ema Gomes (Grupo 3)
- Gonçalo Figueiredo (Grupo 4)
- **Guilherme Lopes (Grupo 5)**

> **Dia 14/11/2022 - CRIAÇÃO DE GRUPO QUALIDADE**

> **QUALIDADE:** <br>
Em Qualidade, os membros estão encarregues da parte dos documentos formais e da organização do repositório (Project Management). São estes que trabalham para que todos os grupos tenham os documentos organizados de forma uniforme e que verificam os processos de testagem e confirmação do produto. <br>

Membros:
- Daniela Mendes (Grupo 1)
- Francisco Ventura (Grupo 1)
- Andreia Silva (Grupo 2)
- Daniela China (Grupo 2)
- Mariana Pombo (Grupo 3)
- Rita Simões(Grupo 3)
- Filipe Viana (Grupo 4)
- Gonçalo Pires (Grupo 4)
- João Simões (Grupo 5)
- Margarida Figueiredo (Grupo 5)

### 2.3.Outros
> Os restantes membros da PL a quem não foi atribuído um cargo específico dos grupos transversais mantiveram-se responsáveis por cumprir funções mais gerais de cada grupo inicial e auxiliar em qualquer área quando necessário. <br>

- Marco Braga (Grupo 1)
- José Porto (Grupo 2)
- João Mendes (Grupo 4)
- Inês Torres (Grupo 5)

<br>

## 3. O Projeto

### 3.1.Enunciado
O projeto deste ano consiste numa plataforma em que os utilizadores possam aceder, criar, resolver e rever quizzes sobre a disciplina de ES. O utilizador poderá criar uma conta na plataforma para que o seu desempenho e atividade na mesma sejam registadas. Para usufruir da feature de criar e resolver, o utilizador deve rever três quizzes. O utilizador não pode rever os seus próprios quizzes. Os quizzes são compilados em testes para outros utilizadores resolverem. Há uma página para ver o seu “score” (Hall of Fame).

### 3.1.1.Requisitos
Com base na informação recolhida em aula teórica:

> - "Uma plataforma de testes com quizzes sobre a disciplina;
> - Quiz: uma pergunta com 6 respostas;
> - Todas as respostas devem ter uma justificação de porque é que estão certas/erradas;
> - Cada teste deve ter 4 quizzes (numa primeira versão, 1 quiz = 1 teste);
> - Login;
> - Quem avalia não pode alterar o quiz;
> - Quizzes não são aleatórios por teste;
> - O user deve avaliar quizzes, de modo a poder aceder à funcionalidade de criar quizzes;
> - Um quiz só pode ser aprovado¬ e colocado na plataforma após a aprovação de 3 users;
> - O quiz tem que estar completo;
> - Há uma página para ver o desempenho do utilizador/estado do website
> - (...);”


**DIA 04/11/2022 - ATUALIZAÇÃO DE REQUISITOS**

Através do grupo ES2022, no canal #general, no dia 04/11/2022, o docente Mário Rela informou todas as PLs por mensagem sobre novos requisitos por parte do cliente, a ser implementadas depois das primeiras serem devidamente incluídas na plataforma:

> - Os quizzes deviam encaixar-se em 12 TAGs definidas pelo docente;
> - Um teste consiste num conjunto de quizzes (N = 20).
> - Cada quizz só pode estar incluído, no máximo, em dois testes.
> - A criação de testes implica selecionar as tags dos temas que se deseja incluir no teste. Por exemplo, selecionando apenas as tags “PM e REQ” a aplicação vai à lista de quizzes disponíveis e escolhe aleatoriamente 10 (N/2) quizzes que tenham a categoria PM e 10 (N/2) quizzes que tenham a categoria REQ (usem aritmética de inteiros).
> - Se não houver quizzes disponíveis de uma dada categoria, o número de quizzes de um tipo ‘em falta’ passa para as restantes categorias, por qualquer ordem.
> - Se não houver nenhum quizz em nenhuma das categorias seleccionadas, o teste não é criado e o criador  é notificado disso.
> - Quando um teste acaba de ser criado, o criador de testes tem de poder saber qual a distribuição efectiva de quizzes por tags (e.g. 10xREQ, 5xPM, 3xIMP, 2xTST). vii. Aliás, esse ‘perfil’ de tags tem de fazer parte da descrição do teste para os ‘resolvedores’ poderem escolher os testes que querem realizar.
> - Um teste tem de ter um ‘título’ (e.g. “Teste Geral”, “Anda testar se és um dev”, “Teste para frontenders”, “Teste do Pixinguinha”,...)
> - O criador de testes não vê as questões que foram incluídas, apenas vê o perfil de tags (e o título que ele próprio deu).
> - O utilizador pode selecionar um teste e resolvê-lo e ver a sua autoavaliação no Hall of Fame
> - O utilizador pode ver a sua classificação e desempenho no seu perfil
> - O administrador pode exportar testes da plataforma que está a usar;
> - O administrador pode importar testes de outras plataformas para a que está a usar.


### 3.1.2.Requisitos para User Stories
Após a recolha de requisitos do cliente para o projeto, a PL resumiu em 6 User Stories essenciais para o desenvolvimento das features pedidas. Estas User Stories auxiliaram a divisão da turma nos grupos iniciais já referidos anteriormente, no capítulo 3. 1.1..

* **USER STORY 1 (atribuída ao GRUPO 1)**  <br> 
“Enquanto autor, quero registar um quiz para ser revisto.”

* **USER STORY 2 (atribuída ao GRUPO 2)**  <br> 
“Enquanto revisor, quero rever um quizz para dar seguimento a esse mesmo quiz.”

* **USER STORY 3 (atribuída ao GRUPO 3)**  <br> 
“Como utilizador autenticado quero ver a solução às respostas do quizz que realizei de forma a que forneça os conhecimentos pretendidos.”

* **USER STORY 4 (atribuída ao GRUPO 4)**  <br> 
“Enquanto utilizador autenticado, pretendo resolver as questões do quiz de forma a testar os meus conhecimentos.”

* **USER STORY 5 (atribuída ao GRUPO 5)**  <br> 
“Enquanto utilizador autenticado quero uma landing page para aceder aos meus status e ações.”

* **USER STORY 6 (atribuída ao GRUPO 5)**  <br> 
“Enquanto utilizador quero fazer login/registar para aceder à Landing Page.”  <br> 
> **DIA 04/11/2022 - ATUALIZAÇÃO DE REQUISITOS**

* **USER STORY 7 (atribuída ao GRUPO 1)**  <br> 
Como criador de quizzes quero poder seleccionar uma ou mais tags --de um conjunto pré-definido-- aquando da criação de um quizz para que os testes possam ser criados com base nessas tags

* **USER STORY 8 (atribuída ao GRUPO 4)**
Como criador de testes quero poder criar um teste com base em tags para que esta comunidade possa fazer auto-avaliação e eu me sentir um boss por ter sido eu a criar um teste que todos os meus colegas vão fazer

* **USER STORY 9 (atribuída ao GRUPO 5)** <br> 
Como resolvedor quero poder seleccionar um teste e resolvê-lo para fazer a minha auto-avaliação e, quem sabe, ir para o top10 do ‘hall of fame’

* **USER STORY 10 (atribuída ao GRUPO 5)**  <br> 
Como resolvedor quero ter no meu perfil o número de quizzes de uma dada categoria (tags) que acertei> para saber quais os meus pontos fortes sim, se um quizz tiver mais que uma tag, ao acertar nesse quizz acrescento pontos em mais que uma categoria

* **USER STORY 11 (atribuída ao GRUPO 2)**  <br> 
Como admin quero poder exportar quizzes em XML para ser possivel exportar quizzes feitos na minha PL --claramente os melhores!-- para a aplicação de outras turmas

* **USER STORY 12 (atribuída ao GRUPO 2)**  <br> 
Como admin quero poder importar quizzes em XML para ser possivel importar quizzes criados noutra PL para a minha aplicação --que é muito mais fixe que as das outras turmas--

### 3.2. Gestão de Projeto 
No início do projeto, cada grupo geral da PL estava encarregue de gerenciar os seus ficheiros relativos ao projeto, como User Stories, Casos de Uso e Testes. Também ficou estipulado desde do inicio como cada membro da PL em geral deveria verificar o seu trabalho a nível individual, registando e documentando todos os contributos através do GitLab e de ficheiros para consulta; para auxiliar essa formalização, os grupos também confirmavam se as contribuições dos seus membros estavam adequadamente registadas, nomeadamente “issues”, “commits” e páginas pessoais.  Alguns membros dos grupos da PL não tinham funções nos grupos transversais criados inicialmente, logo ficava-lhes encarregue de tratar mais da gerência neste sentido. <br>
No dia  14/11/2022,  foi decidido pela PL4 criar o grupo transversal da “Qualidade”, composta por grande parte de membros que ainda não pertenciam a outro grupo para além do inicial, assumindo as funções ditadas anteriormente e inclusive a tarefa de verificar tudo o que fosse relativo a documentos, verificação de testes, análise de issues e commits, e tudo o que tenha a ver com a qualidade do projeto final em termos de documentação.
Para a devida organização da PL, foram criadas várias plataformas de comunicação entre membros e grupos:

- O Repositório foi criado na primeira semana pelo aluno Guilherme Costa onde são colocados os documentos oficiais devidamente formalizados e concluídos do projeto “QuizzES”.
- Inicialmente foi criado um canal no SLACK intitulado PL4 para a comunicação da PL em geral, e grupos no Whatsapp para grupos transversais;
- Numa fase mais posterior, a turma decidiu criar um canal de Discord “QuizzES”, para uma troca mais rápida de informações, dúvidas rápidas, discussões, reuniões e ficheiros incompletos e para verificar.

  

A nível de reuniões, a PL4  convoca reunião geral caso haja alguma nova informação relativa ao projeto, updates do estado do trabalho ou decisões por tomar que não puderam ser tratadas em tempo de aula (segunda-feira, na aula prática). Fora isso, a comunicação/trabalho é gerido entre grupos, quer seja em reuniões de menor escala (subgrupos) ou até mesmo diálogo nos respetivos canais criados no Discord e Whatsapp. Apesar de ser necessário haver atas ou outros documentos que formalizam a informação, todas as decisões, informações e avisos são sempre feitos de forma informal no Discord da PL.

### 3.2.1.Repositório GitLab 
#### [DEV] Código <br>
> Pasta destinada aos ficheiros relativos ao código do projeto. Cada tipo de ficheiro tem uma pasta específica. Contém um README.md com um glossário sobre esses mesmos ficheiros. <br>

**Conteúdo:** <br>
Pastas >> 
- CSS 
- JS 
- JSON 
- PARTS (Pasta de código comum das várias páginas do website) 
- PHP 

#### [PM] Gestão Projeto 
> Pasta destinada aos ficheiros relativos à gestão do projeto: Perfis Individuais e Atas. Contém um README.md com um glossário sobre esses mesmos ficheiros. Contém um README.md com um glossário sobre esses mesmos ficheiros. 

**Conteúdo:** <br>
Pastas >> 
* Atas 
* Profiles 

Ficheiros >> 

#### [REQ] Requesitos 
> Pasta destinada aos ficheiros relativos a requisitos de cada grupo: User Stories, Casos de Uso, Mockups . Contém um README.md com um glossário sobre esses mesmos ficheiros. 

**Conteúdo:** <br>
Pastas >> 
* Grupo 1 
* Grupo 2 
* Grupo 3 
* Grupo 4 
* Grupo 5 

#### [ARCH] Arquitetura 
> Pasta destinada aos ficheiros relativos à base de dados. Contém um README.md com informações sobre esses mesmos ficheiros. 

#### [QA] Testes 
> Pasta destinada aos ficheiros relativos à testagem do código . Contém um README.md com um glossário sobre esses mesmos ficheiros. 

**Conteúdo:** <br> 
Pastas >> 
* Scripts 
* Testes 
* README.md 
 
Ficheiro README.md geral que dá a conhecer os membros e os seus respetivos cargos no projeto.

### 3.2.2.Requisitos
### 3.2.2.1.User Stories
Tendo em conta os requisitos do cliente, cada grupo da PL ficou encarregue de criar as suas User Stories da maneira mais adequada, atendendo às necessidades do cliente (ver 4.1.2).

### 3.2.2.2.Casos de Uso e Testes
Atendendo aos UserStories, cada grupo da PL criou os seus casos de uso referentes às features de que ficaram encarregues: https://gitlab.com/es-pl4/projeto-es/-/tree/main/%5BREQ%5D%20Requisitos

### 3.2.3. Arch
Os grupos transversais com mais presença de trabalho neste departamento são os de “Qualidade” e “Scrums”. A arquitetura consiste na organização do projeto, como os softwares a utilizar, a estrutura de trabalho em esquemas e manuais para consulta, e os elementos essenciais para criar um trabalho eficiente.

### 3.2.4. Development 
Os grupos transversais com mais presença de trabalho neste departamento são os de IDENTIDADE e Implementação. Numa fase inicial, o grupo de implementação aplica o código relativo à estética e funcionalidade consoante a decisão final estipulada pelo grupo de identidade. Os grupos encarregues na implementação têm que trabalhar coletivamente para otimizar o workflow do projeto.

### 3.2.5.Deployment 
Os grupos transversais com mais presença de trabalho neste departamento são os deploys. Os deploys participam com um trabalho ativo na elaboração das features do  projeto e estão encarregues de fazer o merge do código todo para ficar tudo coeso e em funcionamento. Para além disto são os responsáveis por grande parte de resolução de erros do sistema, para poder fazer o seu deploy para o server num estado 100% funcional.

### 3.2.6.Outros 

### 3.2.7.Templates

Aqui estão englobados os templates que devem ser usados para cada tipo de documento/contribuição do projeto e devem ser aplicados por todos os membros da PL4.

#### 3.2.7.1. ATAS
As atas devem ser elaboradas por um membro que esteja presente na reunião em questão e revistas por outro igualmente presente. Deve-se também informar as decisões tomadas na reunião de forma informal no canal de Discord, principalmente para as pessoas interessadas nestas mesmas alterações.
* **(copiar para ficheiro MD (não esquecer de colocar “.md” no título do ficheiro) - o texto entre parênteses são comentários que devem ser apagados)**

> # [ATA] REUNIÃO
> 
> **DIA:** DD - MM - AAAA 
> **DURAÇÃO:** (horas)H(minutos)min 
> **ESCRITO POR:** 
> **REVISTO POR:** 
> 
> ### PARTICIPANTES
> * (MEMBRO)
> * (MEMBRO)
> * (MEMBRO)
> 
> ### PAUTA (assuntos tratados durante a reunião)
> 
> * (Assunto)
> * (Assunto)
> * (Assunto - descrição)
> (descrição é opcional, se servir para explicar melhor o assunto)
> 
> ### ASSUNTOS A SEREM TRATADOS
> 
> * (Tópico)
> * (Tópico)
> * (Tópico)
> (Aqui pode haver ainda distinção consoante a urgência/importância)
> 
> **PL/PROFESSOR** (colocada apenas quando há questões para o professor)
> * (Topico)
> 
> #### Engenharia Software 2022 - PL4 - QuizzES


#### 3.2.7.2.Issues Gerais 
> * (Título à escolha)
> - _Titulos gerais: Reunião Geral; Reunião Grupo x, Ata de Reunião - Grupo x; Revisão de Ata - Grupo x --;_ 
> * (Descrição completa sobre a issue)
> * (/estimate) – opcional 
> * (/spend) - obrigatório 
> * (labels) - obrigatório <br>
> _LABELS ATUAIS: [ARCH], [ATAS], [BACKEND], [DEV], [DEPLOYS], [DOCUMENTOS], [Geral], [Gestão de ficheiros], [GRUPO X], [Identidade], [Implementação], [PM], [Pronto de Testar], [Pronto a Verificar], [REQ], [Reunião], [Scrums], [Verificado], [WIP], [DONE], [Qualidade], [TO-DO], [FRONTEND]_

Nota: Nas issues de atas, documentos, etc, deve estar incluido o link do ficheiro guardado no repositório na descrição.

#### 3.2.7.3. COMMITS

> Títulos: 
> * (CRIAÇÃO_) <br>
> _ex: CRIAÇÃO_[REQ]CasoDeUso_1.MD_ 
> * (ATUALIZAÇÃO_) <br>
> _ex: ATUALIZAÇÃO_PaginaPessoal.MD_ 
> * (EXCLUSÃO_) <br>
> _ex: EXCLUSÃO_ATA_Reunião_Geral.MD_ 
> Outros: 
> * (VERBO_) <br>
> Descrição (em alguns casos): 
> (Descrever o contributo da commit em questão)
 

Nota: As commits devem ser relacionadas a issues, colocando o link na descrição.
 
#### 3.2.7.4. SCRIPTS

(ficheiro MD)
> ## TESTE DE QUALIDADE
> 
> **USER STORY/FEATURE:** 
> **CASO DE USO:** 
> **ESPECIFICAÇÃO DO TESTE:** 
> **TESTER:** 
> **Nº DE TESTE:** 
> **DATA:** 
> 
> | AÇÃO | ESPERADO | RESUTADO (OK/NOK) |
> 
> |--|--|--|
> 
> | (aqui escreve-se o que o tester deve fazer) | (aqui escreve-se a resposta do sistema esperada) | (aqui escreve-se OK ou NOK para dizer se aconteceu ou não||--|--|


#### 3.2.7.5. User Story

> ## User Story (nº da user story)
> ...


#### 3.2.7.6. Caso De Uso

> ## (nome do caso de uso)
> 
> ## Primary Actor:
> 
> ...
> 
> ## Preconditions:
> 
> ...
> 
> ## Success Guarantee:
> 
> ...
> 
> ## Main Success Scenario:
> 
> 1.  ...
> 2.  ...
> 3.  ...
> 4.  ...
> 5.  ...
> 
> ## Extensions:
> 
> -   1a. ...
> -   1b. ...
> 
> ## Exception:
> 
> -   1a. ...
> -   1b. ...
> -   2a. ...


### 3.2.8.Nomenclaturas

As nomenclaturas servem para a documentação e a organização desta. Esta informação está também disponivel em ficheiros README.md nas respetivas pastas no repositório da disciplina para consulta rápida.

#### 3.2.8.1. [DEV] Código

> **HTML** <br>
> [DEV]NomeDaPagina-Vx.html 
> _Ex: [DEV]LoginPage-V1.html_ 
> 
> **CSS** <br>
> [DEV]NomeDaPagina-Vx.css 
> _Ex: [DEV]LoginPage-V1.css_ 
> 
> **JS** <br>
> [DEV]NomeDaPagina-Vx.js 
> _Ex: [DEV]LoginPage-V1.js_ 
> 
> **PARTS** <br>
> [DEV]NomeDaFuncionalidade-Vx.(tipo) 
> _EX: [DEV]Header-V1.css_ 
> 
> **PHP** <br>
> [DEV]NomeDaPagina-Vx.php 
> _Ex: [DEV]LoginPage-V1.php_ 


#### 3.2.8.2.[PM] Gestão Projeto 

> **ATAS** <br>
> [PM]Ata.yyyy-mm-dd.TipoDeReunião 
> _Ex: [PM]Ata.2022-11-02.Geral_ 
> Outros tipos: Grupo-x, Scrums, Deploy, etc 
> 
> **PROFILES** <br>
> [PM]NomeApelido.md 

#### 3.2.8.3. [REQ] Requisitos 
> 
> **USER STORY:** <br>
> [REQ]NúmeroDaStory.UserStory.md 
> _Ex: [REQ]2.UserStory.md_ 
> 
> **CASO DE USO:** <br> 
> [REQ]NúmeroDaStory.NúmeroDoCaso.CasoDeUso.md 
> _Ex: [REQ]2.1.CasoDeUso.md_ 
> 
> **TESTES:** <br>
> [REQ]NúmeroDaStory.NúmeroDoCaso.CasoDeUso.Teste.md 
> _Ex: [REQ]2.1.CasoDeUso.Teste.md_ 
> 
> **MOCKUPS:** <br>
> [REQ]NúmeroDaStory.Mockup.png 
> _Ex: [REQ]2.Mockup.png_ 


#### 3.2.8.4.[QA] Testes 

> **SCRIPTS** <br>
> [QA]_X.x.CDU.script.X.md 
> _Ex: QA_2.2.CDU.script.1.md_ 
> 
> **Testes** <br>
> [QA]_X.x.CDU.script.X_run.X 
> _Ex: QA_2.2.CDU.script.1_run2.md_ 

### 3.3. QuizzES 
#### 3.3.1. O Produto
O nome “QuizzES” vem do conceito da plataforma desenvolvida para quizzes, com a relevância nas últimas duas letras - “E” e “S” -  que são usadas como abreviatura para a cadeira do 3º ano para qual esta mesma foi criada. 

* **QuizzES:** quizzes.dei.uc.pt 
(Servidor interno do DEI - precisa estar na rede do departamento para o aceder) 
(LINK) Arquitetura 

* **Base de dados:** 
- https://gitlab.com/es-pl4/projeto-es/-/blob/main/%5BARCH%5D%20Arquitetura/estrutura_dados.png; 
- https://gitlab.com/es-pl4/projeto-es/-/blob/main/%5BARCH%5D%20Arquitetura/pgAdmin_2.jpg 

* (link) Manual de normas 

#### 3.3.2. Qualidade do Produto 
Para aprovação do projeto, foi elaborado pela equipa de Qualidade um processo de testagem pelo qual elementos como código deveriam passar para garantir resultados provados e de sucesso: <br> 
“(Para termos práticos: DEV - membro que desenvolveu o objeto a ser testado; TESTER - qualquer membro do projeto que se propõe a testar; QUALIDADE - membro do grupo da qualidade)
1. O DEV que acabou de inserir o código abre uma issue _PRONTA A TESTAR_ para experimentar o que está feito.

> Nomenclatura da issue: _“TESTE – feature / caso de uso”_
Descrição especifica do teste (“teste de feature”, “quero que testem botão x”, etc)

2. Um TESTER vê a issue, dá assign a si e faz o teste;
3. Insere o script do teste feito na pasta QA do repositório;
4. Cria um issue para _BUG FIX_, (deve ter a label _TO-DO e BUG FIX_)

> Nomenclatura da issue: _“BUG FIX – feature/caso de uso”_
Descrição - na descrição uma checklist dos bugs notados e linked items (o documento do teste na pasta QA)

5. Os DEVs que querem corrigir bugs devem ir ao board do GITLAB e verificar a coluna de _TO DO_ para ver quais bugs têm que ser resolvidos
6. O DEV assume a issue _BUG FIX_ e resolve com base no teste feito linkado
7. O DEV cria uma issue aberta de _PRONTA A TESTAR_ para refazer o teste

> Nomenclatura da issue: _“TESTE – feature / caso de uso”_
Descrição especifica do teste (“teste de feature”, “quero que testem botão x”, ou também o teste que já foi feito anteriormente como referência)

8. O TESTER torna a repetir o processo (passo 2 e 3)
9. **AO SER APROVADO:** o TESTER cria uma issue de _PRONTA A VERIFICAR_ para uma pessoa da QUALIDADE verificar se o processo está feito
10. A QUALIDADE verifica o processo e termina com _DONE_ e _VERIFICADO_ na issue.”

O grupo de qualidade também disponibilizou SCRIPTS-base para a testagem das features conhecidas, englobadas na pasta [QA] Testes > Scripts, para que qualquer membro pudesse aceder e dar o seu contributo (colocado posteriormente em [QA] Testes > Testes). 
(para consulta: https://gitlab.com/es-pl4/projeto-es/-/blob/main/%5BQA%5D%20Testes/README.md)  <br>
Para restantes elementos relativos ao funcionamento dos grupos, como issues, commits, rotulação com labels, a Qualidade definiu um pequeno Q&A sobre estes assuntos, respondidos logo após a criação do grupo (https://gitlab.com/es-pl4/projeto-es/-/blob/main/%5BPM%5D%20Gest%C3%A3o%20Projeto/Q%26A_Qualidade%20_19-11-2022.md). <br>
Cada membro deve fazer o melhor para cumprir os parâmetros definidos, mas o grupo de Qualidade tem também a função de verificar estes elementos quando possivel e alertar para qualquer erro que detete.

## 4. Anexos

### 4.1. Contactos
 
Nome: Ana Pimentel <br>
Número: 2020224509 <br>
Email: anamicaelapimentel@gmail.com <br>

Nome: Andreia Silva <br>
Número: 2020246205 <br>
Email: andreiaferrsilva@gmail.com <br>

Nome: Catarina Anselmo <br>
Número: 2020166592 <br>
Email: cataanselmo@gmail.com <br>

Nome: Constança Correia <br>
Número: 2020222539 <br>
Email: constancamfcorreia@gmail.com <br>

Nome: Daniela China <br>
Número: 2020218665 <br>
Email: danielachina22@gmail.com <br>

Nome: Daniela Mendes <br>
Número: UC2020230212 <br>
Email: danielamendes712@gmail.com <br>

Nome: Débora Patrício <br>
Número: 2020228290 <br>
Email: margaridadebora@gmail.com <br>

Nome: Diogo Silva <br>
Número: 2020210851 <br>
Email: diogo.g.d.silva@outlook.pt <br>

Nome: Ema Gomes <br>
Número: 2020213854 <br>
Email: emagomes@student.dei.uc.pt <br>

Nome: Ester Gaio <br>
Número: 2020241130 <br>
Email: estergoreti@gmail.com <br>

Nome: Filipa Ferreira <br>
Número: 2020231259 <br>
Email: filipaferreira227@gmail.com <br>

Nome: Filipe Viana <br>
Número: 2019216279 <br>
Email: filipe7vv@gmail.com <br>

Nome: Filipa Vilhena <br>
Número: 2020226038 <br>
Email: filipavilhenana@gmail.com <br>

Nome: Francisco Ventura <br>
Número: UC2020241389 <br>
Email: franciscoventura555@gmail.com <br>

Nome: Gonçalo Figueiredo <br>
Número: 2020236149 <br>
Email: gfigueiredo@student.dei.uc.pt <br>

Nome: Gonçalo Pires <br>
Número: 2020236720 <br>
Email: goncaloppires01@gmail.com <br>

Nome: Guilherme Almeida <br>
Número: 2020236588 <br>
Email: uc2020236588@student.uc.pt <br>

Nome: Guilherme Costa <br>
Número: 2019211271 <br>
Email: guilhermepmcosta@gmail.com <br>

Nome: Guilherme Lopes <br>
Número: 2020239080 <br>
Email: guilhermecapelo@gmail.com <br>

Nome: Inês Torres <br>
Número: 2019214776 <br>
Email: inestsilva2001@hotmail.com <br>

Nome: João Mendes <br>
Número: - <br>
Email: - <br>

Nome: João Simões <br>
Número: 2020212694 <br>
Email: jx.simoes@outlook.pt <br>

Nome: José Porto <br>
Número: 2018306203 <br>
Email: josefmporto@gmail.com <br>

Nome: Mafalda Almeida <br>
Número: 2020217073 <br>
Email: mafaldaferalmeida@gmail.com <br>

Nome: Marco Braga <br>
Número: 2019208948 <br>
Email: marco.braga2001@gmail.com <br>

Nome: Margarida Figueiredo <br>
Número: UC2020236731 <br>
Email: margaridatonelfigueiredo@gmail.com <br>

Nome: Margarida Mendonça <br>
Número: 2020239976 <br>
Email: mmargarim@gmail.com <br>

Nome: Maria Lima <br>
Número: 2019112625 <br>
Email: massil_lima@hotmail.com <br>

Nome: Mariana Pombo <br>
Número: 2019223174 <br>
Email: pombomariana7@gmail.com <br>

Nome: Pedro Monteiro <br>
Número: 2019215805 <br>
Email: pnmonteiro@student.dei.uc.pt <br>

Nome: Rita Simões <br>
Número: 2020222133 <br>
Email: ritasimoes2727@gmail.com <br>

Nome: Rui Moniz <br>
Número: 2019225770 <br>
Email: rui.p.moniz.000@gmail.com <br>

Nome: Tomás Oliveira <br>
Número: 2020240926 <br>
Email: tomasten9@gmail.com <br>

Nome: Tomás Ferreira <br>
Número: 2020219447 <br>
Email: tommatiasferreira@gmail.com <br>

### 4.2. Plataformas 
* GITLAB: https://gitlab.com/es-pl4/projeto-es.git <br>
* DISCORD: https://discord.gg/KtyBrFM3 <br>
* SLACK: https://es2022workspace.slack.com/archives/C042HBW3136 <br>








